#include "stdafx.h"
#include "shader.h"


Shader::Shader(const std::string& filename)
    : program(0)
{
    const auto vert = CreateStage(gl::VERTEX_SHADER, filename);
    const auto frag = CreateStage(gl::FRAGMENT_SHADER, filename);
    
    if (!vert || !frag)
        throw "Shader::Shader() Error: Stage compilation failed.";

    program = gl::CreateProgram();
    gl::AttachShader(program, *vert);
    gl::AttachShader(program, *frag);
    gl::LinkProgram(program);

    GLint success;
    GLchar infoLog[1024];
    gl::GetProgramiv(program, gl::LINK_STATUS, &success);
    if (!success)
    {
        gl::GetProgramInfoLog(program, 1024, nullptr, infoLog);
        std::cout << "Shader::Shader() Error: " << infoLog << '\n';
        throw "dicks";
    }

    gl::DeleteShader(*vert);
    gl::DeleteShader(*frag);
}

GLint Shader::GetUniformLoc(const std::string& name) const
{
    return gl::GetUniformLocation(program, name.c_str());
}

void Shader::Use() const
{
    gl::UseProgram(program);
}

void Shader::SetMat4(const GLint loc, const glm::mat4& val)
{
    gl::UniformMatrix4fv(loc, 1, gl::FALSE_, &val[0][0]);
}

void Shader::SetMat4(const std::string& locName, const glm::mat4& val) const
{
    SetMat4(GetUniformLoc(locName), val);
}

void Shader::SetInt(const GLint loc, const int val)
{
    gl::Uniform1i(loc, val);
}

void Shader::SetInt(const std::string& locName, const int val) const
{
    SetInt(GetUniformLoc(locName), val);
}

void Shader::SetFloat(const GLint loc, const float val)
{
    gl::Uniform1f(loc, val);
}

void Shader::SetFloat(const std::string& locName, const float val) const
{
    SetFloat(GetUniformLoc(locName), val);
}

void Shader::SetVec2(const GLint loc, const glm::vec2& val)
{
    gl::Uniform2fv(loc, 1, &val[0]);
}

void Shader::SetVec2(const std::string& locName, const glm::vec2& val) const
{
    SetVec2(GetUniformLoc(locName), val);
}

void Shader::SetVec3(const GLint loc, const glm::vec3& val)
{
    gl::Uniform3fv(loc, 1, &val[0]);
}

void Shader::SetVec3(const std::string& locName, const glm::vec3& val) const
{
    SetVec3(GetUniformLoc(locName), val);
}

std::string Shader::ReadFile(const std::string& filename)
{
    std::ifstream file(filename);
    std::stringstream stream;
    stream << file.rdbuf();
    return stream.str();
}

std::optional<GLuint> Shader::CompileStage(const GLenum stage, const char* source)
{
    const auto shader = gl::CreateShader(stage);
    gl::ShaderSource(shader, 1, &source, nullptr);
    gl::CompileShader(shader);

    GLint success;
    GLchar infoLog[1024];
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &success);
    if (!success)
    {
        gl::GetShaderInfoLog(shader, 1024, nullptr, infoLog);
        std::cout << "Shader::CompileStage() Error: " << infoLog << '\n';
        return {};
    }

    return shader;
}

std::optional<GLuint> Shader::CreateStage(const GLenum stage, const std::string& filename)
{
    static const std::map<GLenum, const char*> exts = {
        { gl::VERTEX_SHADER, ".vert" },
        { gl::FRAGMENT_SHADER, ".frag" }
    };

    return CompileStage(stage, ReadFile("assets/" + filename + exts.at(stage)).c_str());
}
