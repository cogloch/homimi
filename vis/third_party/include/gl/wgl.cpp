#include "stdafx.h"
#include <algorithm>
#include <string.h>
#include "wgl.hpp"

#if defined(__APPLE__)
#include <dlfcn.h>

static void* AppleGLGetProcAddress (const char *name)
{
	static void* image = NULL;
	
	if (NULL == image)
		image = dlopen("/System/Library/Frameworks/OpenGL.framework/Versions/Current/OpenGL", RTLD_LAZY);

	return (image ? dlsym(image, name) : NULL);
}
#endif /* __APPLE__ */

#if defined(__sgi) || defined (__sun)
#include <dlfcn.h>
#include <stdio.h>

static void* SunGetProcAddress (const GLubyte* name)
{
  static void* h = NULL;
  static void* gpa;

  if (h == NULL)
  {
    if ((h = dlopen(NULL, RTLD_LAZY | RTLD_LOCAL)) == NULL) return NULL;
    gpa = dlsym(h, "glXGetProcAddress");
  }

  if (gpa != NULL)
    return ((void*(*)(const GLubyte*))gpa)(name);
  else
    return dlsym(h, (const char*)name);
}
#endif /* __sgi || __sun */

#if defined(_WIN32)

#ifdef _MSC_VER
#pragma warning(disable: 4055)
#pragma warning(disable: 4054)
#pragma warning(disable: 4996)
#endif

static int TestPointer(const PROC pTest)
{
	ptrdiff_t iTest;
	if(!pTest) return 0;
	iTest = (ptrdiff_t)pTest;
	
	if(iTest == 1 || iTest == 2 || iTest == 3 || iTest == -1) return 0;
	
	return 1;
}

static PROC WinGetProcAddress(const char *name)
{
	PROC pFunc = wglGetProcAddress((LPCSTR)name);
	if(TestPointer(pFunc))
	{
		return pFunc;
	}
	HMODULE glMod = GetModuleHandleA("OpenGL32.dll");
	return (PROC)GetProcAddress(glMod, (LPCSTR)name);
}
	
#define IntGetProcAddress(name) WinGetProcAddress(name)
#else
	#if defined(__APPLE__)
		#define IntGetProcAddress(name) AppleGLGetProcAddress(name)
	#else
		#if defined(__sgi) || defined(__sun)
			#define IntGetProcAddress(name) SunGetProcAddress(name)
		#else /* GLX */
		    #include <GL/glx.h>

			#define IntGetProcAddress(name) (*glXGetProcAddressARB)((const GLubyte*)name)
		#endif
	#endif
#endif

namespace wgl
{
	namespace exts
	{
		bool var_ARB_multisample = false;
		bool var_ARB_extensions_string = false;
		bool var_ARB_pixel_format = false;
		bool var_ARB_pixel_format_float = false;
		bool var_ARB_framebuffer_sRGB = false;
		bool var_ARB_create_context = false;
		bool var_ARB_create_context_profile = false;
		bool var_ARB_create_context_robustness = false;
		bool var_EXT_swap_control = false;
		bool var_EXT_pixel_format_packed_float = false;
		bool var_EXT_create_context_es2_profile = false;
		bool var_EXT_swap_control_tear = false;
		bool var_NV_swap_group = false;
	}
	
	// Extension: ARB_extensions_string
	typedef const char * (CODEGEN_FUNCPTR *PFNGETEXTENSIONSSTRINGARBPROC)(HDC);
	
	// Extension: ARB_pixel_format
	typedef BOOL (CODEGEN_FUNCPTR *PFNCHOOSEPIXELFORMATARBPROC)(HDC, const int *, const FLOAT *, UINT, int *, UINT *);
	typedef BOOL (CODEGEN_FUNCPTR *PFNGETPIXELFORMATATTRIBFVARBPROC)(HDC, int, int, UINT, const int *, FLOAT *);
	typedef BOOL (CODEGEN_FUNCPTR *PFNGETPIXELFORMATATTRIBIVARBPROC)(HDC, int, int, UINT, const int *, int *);
	
	// Extension: ARB_create_context
	typedef HGLRC (CODEGEN_FUNCPTR *PFNCREATECONTEXTATTRIBSARBPROC)(HDC, HGLRC, const int *);
	
	// Extension: EXT_swap_control
	typedef int (CODEGEN_FUNCPTR *PFNGETSWAPINTERVALEXTPROC)(void);
	typedef BOOL (CODEGEN_FUNCPTR *PFNSWAPINTERVALEXTPROC)(int);
	
	// Extension: NV_swap_group
	typedef BOOL (CODEGEN_FUNCPTR *PFNBINDSWAPBARRIERNVPROC)(GLuint, GLuint);
	typedef BOOL (CODEGEN_FUNCPTR *PFNJOINSWAPGROUPNVPROC)(HDC, GLuint);
	typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYFRAMECOUNTNVPROC)(HDC, GLuint *);
	typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYMAXSWAPGROUPSNVPROC)(HDC, GLuint *, GLuint *);
	typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYSWAPGROUPNVPROC)(HDC, GLuint *, GLuint *);
	typedef BOOL (CODEGEN_FUNCPTR *PFNRESETFRAMECOUNTNVPROC)(HDC);
	
	
	// Extension: ARB_extensions_string
	PFNGETEXTENSIONSSTRINGARBPROC GetExtensionsStringARB;
	
	// Extension: ARB_pixel_format
	PFNCHOOSEPIXELFORMATARBPROC ChoosePixelFormatARB;
	PFNGETPIXELFORMATATTRIBFVARBPROC GetPixelFormatAttribfvARB;
	PFNGETPIXELFORMATATTRIBIVARBPROC GetPixelFormatAttribivARB;
	
	// Extension: ARB_create_context
	PFNCREATECONTEXTATTRIBSARBPROC CreateContextAttribsARB;
	
	// Extension: EXT_swap_control
	PFNGETSWAPINTERVALEXTPROC GetSwapIntervalEXT;
	PFNSWAPINTERVALEXTPROC SwapIntervalEXT;
	
	// Extension: NV_swap_group
	PFNBINDSWAPBARRIERNVPROC BindSwapBarrierNV;
	PFNJOINSWAPGROUPNVPROC JoinSwapGroupNV;
	PFNQUERYFRAMECOUNTNVPROC QueryFrameCountNV;
	PFNQUERYMAXSWAPGROUPSNVPROC QueryMaxSwapGroupsNV;
	PFNQUERYSWAPGROUPNVPROC QuerySwapGroupNV;
	PFNRESETFRAMECOUNTNVPROC ResetFrameCountNV;
	
	
	// Extension: ARB_extensions_string
	static const char * CODEGEN_FUNCPTR Switch_GetExtensionsStringARB(HDC hdc)
	{
		GetExtensionsStringARB = (PFNGETEXTENSIONSSTRINGARBPROC)IntGetProcAddress("wglGetExtensionsStringARB");
		return GetExtensionsStringARB(hdc);
	}

	
	// Extension: ARB_pixel_format
	static BOOL CODEGEN_FUNCPTR Switch_ChoosePixelFormatARB(HDC hdc, const int * piAttribIList, const FLOAT * pfAttribFList, UINT nMaxFormats, int * piFormats, UINT * nNumFormats)
	{
		ChoosePixelFormatARB = (PFNCHOOSEPIXELFORMATARBPROC)IntGetProcAddress("wglChoosePixelFormatARB");
		return ChoosePixelFormatARB(hdc, piAttribIList, pfAttribFList, nMaxFormats, piFormats, nNumFormats);
	}

	static BOOL CODEGEN_FUNCPTR Switch_GetPixelFormatAttribfvARB(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int * piAttributes, FLOAT * pfValues)
	{
		GetPixelFormatAttribfvARB = (PFNGETPIXELFORMATATTRIBFVARBPROC)IntGetProcAddress("wglGetPixelFormatAttribfvARB");
		return GetPixelFormatAttribfvARB(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, pfValues);
	}

	static BOOL CODEGEN_FUNCPTR Switch_GetPixelFormatAttribivARB(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int * piAttributes, int * piValues)
	{
		GetPixelFormatAttribivARB = (PFNGETPIXELFORMATATTRIBIVARBPROC)IntGetProcAddress("wglGetPixelFormatAttribivARB");
		return GetPixelFormatAttribivARB(hdc, iPixelFormat, iLayerPlane, nAttributes, piAttributes, piValues);
	}

	
	// Extension: ARB_create_context
	static HGLRC CODEGEN_FUNCPTR Switch_CreateContextAttribsARB(HDC hDC, HGLRC hShareContext, const int * attribList)
	{
		CreateContextAttribsARB = (PFNCREATECONTEXTATTRIBSARBPROC)IntGetProcAddress("wglCreateContextAttribsARB");
		return CreateContextAttribsARB(hDC, hShareContext, attribList);
	}

	
	// Extension: EXT_swap_control
	static int CODEGEN_FUNCPTR Switch_GetSwapIntervalEXT(void)
	{
		GetSwapIntervalEXT = (PFNGETSWAPINTERVALEXTPROC)IntGetProcAddress("wglGetSwapIntervalEXT");
		return GetSwapIntervalEXT();
	}

	static BOOL CODEGEN_FUNCPTR Switch_SwapIntervalEXT(int interval)
	{
		SwapIntervalEXT = (PFNSWAPINTERVALEXTPROC)IntGetProcAddress("wglSwapIntervalEXT");
		return SwapIntervalEXT(interval);
	}

	
	// Extension: NV_swap_group
	static BOOL CODEGEN_FUNCPTR Switch_BindSwapBarrierNV(GLuint group, GLuint barrier)
	{
		BindSwapBarrierNV = (PFNBINDSWAPBARRIERNVPROC)IntGetProcAddress("wglBindSwapBarrierNV");
		return BindSwapBarrierNV(group, barrier);
	}

	static BOOL CODEGEN_FUNCPTR Switch_JoinSwapGroupNV(HDC hDC, GLuint group)
	{
		JoinSwapGroupNV = (PFNJOINSWAPGROUPNVPROC)IntGetProcAddress("wglJoinSwapGroupNV");
		return JoinSwapGroupNV(hDC, group);
	}

	static BOOL CODEGEN_FUNCPTR Switch_QueryFrameCountNV(HDC hDC, GLuint * count)
	{
		QueryFrameCountNV = (PFNQUERYFRAMECOUNTNVPROC)IntGetProcAddress("wglQueryFrameCountNV");
		return QueryFrameCountNV(hDC, count);
	}

	static BOOL CODEGEN_FUNCPTR Switch_QueryMaxSwapGroupsNV(HDC hDC, GLuint * maxGroups, GLuint * maxBarriers)
	{
		QueryMaxSwapGroupsNV = (PFNQUERYMAXSWAPGROUPSNVPROC)IntGetProcAddress("wglQueryMaxSwapGroupsNV");
		return QueryMaxSwapGroupsNV(hDC, maxGroups, maxBarriers);
	}

	static BOOL CODEGEN_FUNCPTR Switch_QuerySwapGroupNV(HDC hDC, GLuint * group, GLuint * barrier)
	{
		QuerySwapGroupNV = (PFNQUERYSWAPGROUPNVPROC)IntGetProcAddress("wglQuerySwapGroupNV");
		return QuerySwapGroupNV(hDC, group, barrier);
	}

	static BOOL CODEGEN_FUNCPTR Switch_ResetFrameCountNV(HDC hDC)
	{
		ResetFrameCountNV = (PFNRESETFRAMECOUNTNVPROC)IntGetProcAddress("wglResetFrameCountNV");
		return ResetFrameCountNV(hDC);
	}

	
	
	namespace 
	{
		struct InitializeVariables
		{
			InitializeVariables()
			{
				// Extension: ARB_extensions_string
				GetExtensionsStringARB = Switch_GetExtensionsStringARB;
				
				// Extension: ARB_pixel_format
				ChoosePixelFormatARB = Switch_ChoosePixelFormatARB;
				GetPixelFormatAttribfvARB = Switch_GetPixelFormatAttribfvARB;
				GetPixelFormatAttribivARB = Switch_GetPixelFormatAttribivARB;
				
				// Extension: ARB_create_context
				CreateContextAttribsARB = Switch_CreateContextAttribsARB;
				
				// Extension: EXT_swap_control
				GetSwapIntervalEXT = Switch_GetSwapIntervalEXT;
				SwapIntervalEXT = Switch_SwapIntervalEXT;
				
				// Extension: NV_swap_group
				BindSwapBarrierNV = Switch_BindSwapBarrierNV;
				JoinSwapGroupNV = Switch_JoinSwapGroupNV;
				QueryFrameCountNV = Switch_QueryFrameCountNV;
				QueryMaxSwapGroupsNV = Switch_QueryMaxSwapGroupsNV;
				QuerySwapGroupNV = Switch_QuerySwapGroupNV;
				ResetFrameCountNV = Switch_ResetFrameCountNV;
				
			}
		};

		InitializeVariables g_initVariables;
	}
	
	namespace sys
	{
		namespace 
		{
			void ClearExtensionVariables()
			{
				exts::var_ARB_multisample = false;
				exts::var_ARB_extensions_string = false;
				exts::var_ARB_pixel_format = false;
				exts::var_ARB_pixel_format_float = false;
				exts::var_ARB_framebuffer_sRGB = false;
				exts::var_ARB_create_context = false;
				exts::var_ARB_create_context_profile = false;
				exts::var_ARB_create_context_robustness = false;
				exts::var_EXT_swap_control = false;
				exts::var_EXT_pixel_format_packed_float = false;
				exts::var_EXT_create_context_es2_profile = false;
				exts::var_EXT_swap_control_tear = false;
				exts::var_NV_swap_group = false;
			}
			
			struct MapEntry
			{
				const char *extName;
				bool *extVariable;
			};
			
			struct MapCompare
			{
				MapCompare(const char *test_) : test(test_) {}
				bool operator()(const MapEntry &other) { return strcmp(test, other.extName) == 0; }
				const char *test;
			};
			
			struct ClearEntry
			{
			  void operator()(MapEntry &entry) { *(entry.extVariable) = false;}
			};
			
			MapEntry g_mappingTable[13] =
			{
				{"WGL_ARB_multisample", &exts::var_ARB_multisample},
				{"WGL_ARB_extensions_string", &exts::var_ARB_extensions_string},
				{"WGL_ARB_pixel_format", &exts::var_ARB_pixel_format},
				{"WGL_ARB_pixel_format_float", &exts::var_ARB_pixel_format_float},
				{"WGL_ARB_framebuffer_sRGB", &exts::var_ARB_framebuffer_sRGB},
				{"WGL_ARB_create_context", &exts::var_ARB_create_context},
				{"WGL_ARB_create_context_profile", &exts::var_ARB_create_context_profile},
				{"WGL_ARB_create_context_robustness", &exts::var_ARB_create_context_robustness},
				{"WGL_EXT_swap_control", &exts::var_EXT_swap_control},
				{"WGL_EXT_pixel_format_packed_float", &exts::var_EXT_pixel_format_packed_float},
				{"WGL_EXT_create_context_es2_profile", &exts::var_EXT_create_context_es2_profile},
				{"WGL_EXT_swap_control_tear", &exts::var_EXT_swap_control_tear},
				{"WGL_NV_swap_group", &exts::var_NV_swap_group},
			};
			
			void LoadExtByName(const char *extensionName)
			{
				MapEntry *tableEnd = &g_mappingTable[13];
				MapEntry *entry = std::find_if(&g_mappingTable[0], tableEnd, MapCompare(extensionName));
				
				if(entry != tableEnd)
					*(entry->extVariable) = true;
			}
			
			static void ProcExtsFromExtString(const char *strExtList)
			{
				size_t iExtListLen = strlen(strExtList);
				const char *strExtListEnd = strExtList + iExtListLen;
				const char *strCurrPos = strExtList;
				char strWorkBuff[256];
			
				while(*strCurrPos)
				{
					/*Get the extension at our position.*/
					const char *strEndStr = strchr(strCurrPos, ' ');
					int iStop = 0;
					if(strEndStr == NULL)
					{
						strEndStr = strExtListEnd;
						iStop = 1;
					}
			
					int iStrLen = (int)((ptrdiff_t)strEndStr - (ptrdiff_t)strCurrPos);
			
					if(iStrLen > 255)
						return;
			
					strncpy(strWorkBuff, strCurrPos, iStrLen);
					strWorkBuff[iStrLen] = '\0';
			
					LoadExtByName(strWorkBuff);
			
					strCurrPos = strEndStr + 1;
					if(iStop) break;
				}
			}
		}
		void CheckExtensions(HDC hdc)
		{
			ClearExtensionVariables();
			std::for_each(&g_mappingTable[0], &g_mappingTable[13], ClearEntry());
			
			typedef const char * (CODEGEN_FUNCPTR *MYGETEXTSTRINGPROC)(HDC);
			MYGETEXTSTRINGPROC InternalGetExtensionString = (MYGETEXTSTRINGPROC)IntGetProcAddress("wglGetExtensionsStringARB");
			if(!InternalGetExtensionString) return;
			
			ProcExtsFromExtString((const char *)InternalGetExtensionString(hdc));
		}
	}
}
