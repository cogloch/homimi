#pragma once
#include <functional>
#include <chrono>
#include <vector>

using namespace std::chrono_literals;


inline double TimeCallMs(const std::function<void()> func)
{
    const auto start = std::chrono::steady_clock::now();
    func();
    return std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - start).count();
}

using TimerHandle = int;

// Everything in ms 
struct TimerMgr
{
    TimerMgr();
    TimerHandle AddTimer(const double = 0.0);
    void SetTimer(const TimerHandle, const double);
    bool IsReady(const TimerHandle);
    void Pause(const TimerHandle);
    void Resume(const TimerHandle);
    void Tick();

    struct Timer
    {
        double timeout;
        double currentTimeout;
        bool running;
    };
    std::vector<Timer> timers;
    std::chrono::steady_clock::time_point prevUpdate;
    //std::chrono::duration<double, std::milli> prevUpdate;
};