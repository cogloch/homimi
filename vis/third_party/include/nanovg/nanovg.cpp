#include "stdafx.h"
#include <stdio.h>
#include "nanovg_gl.h"

#include "nanovg.h"
#define FONTSTASH_IMPLEMENTATION
#include "fontstash.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef RBG

#pragma warning(disable: 4100)  // unreferenced formal parameter
#pragma warning(disable: 4127)  // conditional expression is constant
#pragma warning(disable: 4204)  // nonstandard extension used : non-constant aggregate initializer
#pragma warning(disable: 4706)  // assignment within conditional expression

#define NVG_INIT_FONTIMAGE_SIZE  512
#define NVG_MAX_FONTIMAGE_SIZE   2048

#define NVG_INIT_COMMANDS_SIZE 256
#define NVG_INIT_POINTS_SIZE 128
#define NVG_INIT_PATHS_SIZE 16
#define NVG_INIT_VERTS_SIZE 256

#define NVG_KAPPA90 0.5522847493f	// Length proportional to radius of a cubic bezier handle for 90deg arcs.

#define NVG_COUNTOF(arr) (sizeof(arr) / sizeof(0[arr]))


namespace nvg
{
	float modf(float a, float b) { return fmodf(a, b); }
	int mini(int a, int b) { return a < b ? a : b; }
	int maxi(int a, int b) { return a > b ? a : b; }
	int clampi(int a, int mn, int mx) { return a < mn ? mn : (a > mx ? mx : a); }
	float minf(float a, float b) { return a < b ? a : b; }
	float maxf(float a, float b) { return a > b ? a : b; }
	float absf(float a) { return a >= 0.0f ? a : -a; }
	float signf(float a) { return a >= 0.0f ? 1.0f : -1.0f; }
	float clampf(float a, float mn, float mx) { return a < mn ? mn : (a > mx ? mx : a); }
	float cross(float dx0, float dy0, float dx1, float dy1) { return dx1*dy0 - dx0*dy1; }

	float normalize(float* x, float* y)
	{
		float d = sqrtf((*x)*(*x) + (*y)*(*y));
		if (d > 1e-6f) 
		{
			float id = 1.0f / d;
			*x *= id;
			*y *= id;
		}
		return d;
	}


	PathCache::PathCache()
	{
		points.reserve(NVG_INIT_POINTS_SIZE);
		paths.reserve(NVG_INIT_PATHS_SIZE);
		verts.reserve(NVG_INIT_VERTS_SIZE);
	}

	void Context::SetDevicePixelRatio(const float ratio)
	{
		tessTol = 0.25f / ratio;
		distTol = 0.01f / ratio;
		fringeWidth = 1.0f / ratio;
		devicePxRatio = ratio;
	}

	CompositeOperationState compositeOperationState(int op)
	{
		int sfactor, dfactor;

		if (op == NVG_SOURCE_OVER)
		{
			sfactor = NVG_ONE;
			dfactor = NVG_ONE_MINUS_SRC_ALPHA;
		}
		else if (op == NVG_SOURCE_IN)
		{
			sfactor = NVG_DST_ALPHA;
			dfactor = NVG_ZERO;
		}
		else if (op == NVG_SOURCE_OUT)
		{
			sfactor = NVG_ONE_MINUS_DST_ALPHA;
			dfactor = NVG_ZERO;
		}
		else if (op == NVG_ATOP)
		{
			sfactor = NVG_DST_ALPHA;
			dfactor = NVG_ONE_MINUS_SRC_ALPHA;
		}
		else if (op == NVG_DESTINATION_OVER)
		{
			sfactor = NVG_ONE_MINUS_DST_ALPHA;
			dfactor = NVG_ONE;
		}
		else if (op == NVG_DESTINATION_IN)
		{
			sfactor = NVG_ZERO;
			dfactor = NVG_SRC_ALPHA;
		}
		else if (op == NVG_DESTINATION_OUT)
		{
			sfactor = NVG_ZERO;
			dfactor = NVG_ONE_MINUS_SRC_ALPHA;
		}
		else if (op == NVG_DESTINATION_ATOP)
		{
			sfactor = NVG_ONE_MINUS_DST_ALPHA;
			dfactor = NVG_SRC_ALPHA;
		}
		else if (op == NVG_LIGHTER)
		{
			sfactor = NVG_ONE;
			dfactor = NVG_ONE;
		}
		else if (op == NVG_COPY)
		{
			sfactor = NVG_ONE;
			dfactor = NVG_ZERO;
		}
		else if (op == NVG_XOR)
		{
			sfactor = NVG_ONE_MINUS_DST_ALPHA;
			dfactor = NVG_ONE_MINUS_SRC_ALPHA;
		}
		else
		{
			sfactor = NVG_ONE;
			dfactor = NVG_ZERO;
		}

		CompositeOperationState state;
		state.srcRGB = sfactor;
		state.dstRGB = dfactor;
		state.srcAlpha = sfactor;
		state.dstAlpha = dfactor;
		return state;
	}

	State* Context::GetState()
	{
		return &states[nstates - 1];
	}

	// State handling
	void Context::Save()
	{
		if (nstates >= NVG_MAX_STATES)
			return;
		if (nstates > 0)
			memcpy(&states[nstates], &states[nstates - 1], sizeof(State));
		nstates++;
	}

	void Context::Restore()
	{
		if (nstates <= 1)
			return;
		nstates--;
	}

	void paint::SetColor(const color& color)
	{
		memset(this, 0, sizeof(*this));
		TransformIdentity(xform);
		radius = 0.0f;
		feather = 1.0f;
		innerColor = color;
		outerColor = color;
	}

	void State::Reset()
	{
		memset(this, 0, sizeof(*this));

		fill.SetColor(RGBA(255, 255, 255, 255));
		stroke.SetColor(RGBA(0, 0, 0, 255));
		compositeOperation = compositeOperationState(NVG_SOURCE_OVER);
		shapeAntiAlias = 1;
		strokeWidth = 1.0f;
		miterLimit = 10.0f;
		lineCap = NVG_BUTT;
		lineJoin = NVG_MITER;
		alpha = 1.0f;
		TransformIdentity(xform);

		scissor.extent[0] = -1.0f;
		scissor.extent[1] = -1.0f;

		fontSize = 16.0f;
		letterSpacing = 0.0f;
		lineHeight = 1.0f;
		fontBlur = 0.0f;
		textAlign = LEFT | BASELINE;
		fontId = 0;
	}

	void Context::Reset()
	{
		GetState()->Reset();
	}

    Context::Context(const int flags)
        : cache(nullptr)
        , fs(nullptr)
	{
        memset(this, 0, sizeof(Context));
		for (auto& fontImg : fontImages) fontImg = 0;

        glContext = new glnvg::Context();
        glContext->flags = flags;
        edgeAA = flags & ANTIALIAS ? 1 : 0;

		commands.reserve(NVG_INIT_COMMANDS_SIZE);
		cache = new PathCache();
		
		Save();
		Reset();

		SetDevicePixelRatio(1.0f);

        glContext->Create();
        
		// Init font rendering
		const fons::Params fontParams(NVG_INIT_FONTIMAGE_SIZE, NVG_INIT_FONTIMAGE_SIZE, fons::ZERO_TOPLEFT);
		fs = new fons::Context(fontParams);

		// Create font texture
		fontImages[0] = glContext->CreateTexture(NVG_TEXTURE_ALPHA, fontParams.width, fontParams.height, 0, NULL);
		if (fontImages[0] == 0) throw "Context::Context: renderCreateTexture() failed";
		fontImageIdx = 0;
	}

	Context::~Context()
	{
		delete cache;
		delete fs;
		
		for(auto& fontImg : fontImages)
		{
			if(fontImg)
			{
				DeleteImage(fontImg);
				fontImg = 0;
			}
		}

        delete glContext;
	}

	void Context::BeginFrame(int windowWidth, int windowHeight, float devicePixelRatio)
	{
		nstates = 0;
		Save();
		Reset();
		
		SetDevicePixelRatio(devicePixelRatio);

		glContext->SetViewport(windowWidth, windowHeight);

		drawCallCount = 0;
		fillTriCount = 0;
		strokeTriCount = 0;
		textTriCount = 0;
	}

	void Context::CancelFrame()
	{
        glContext->Cancel();
	}

	void Context::EndFrame()
	{
		glContext->Flush();
		if (fontImageIdx == 0) return;


		int fontImage = fontImages[fontImageIdx];
		if (fontImage == 0)
			return;
		int iw, ih;
		ImageSize(fontImage, &iw, &ih);

		int i, j;
		for (i = j = 0; i < fontImageIdx; i++)
		{
			if (fontImages[i] != 0)
			{
				int nw, nh;
				ImageSize(fontImages[i], &nw, &nh);
				if (nw < iw || nh < ih)
					DeleteImage(fontImages[i]);
				else
					fontImages[j++] = fontImages[i];
			}
		}
		// make current font image to first
		fontImages[j++] = fontImages[0];
		fontImages[0] = fontImage;
		fontImageIdx = 0;
		// clear all images after j
		for (i = j; i < NVG_MAX_FONTIMAGES; i++)
			fontImages[i] = 0;
	}

	color nvgRGB(unsigned char r, unsigned char g, unsigned char b)
	{
		return RGBA(r,g,b,255);
	}

	color RGBf(float r, float g, float b)
	{
		return RGBAf(r,g,b,1.0f);
	}

	color RGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
	{
		color color;
		// Use longer initialization to suppress warning.
		color.r = r / 255.0f;
		color.g = g / 255.0f;
		color.b = b / 255.0f;
		color.a = a / 255.0f;
		return color;
	}

	color RGBAf(float r, float g, float b, float a)
	{
		color color;
		// Use longer initialization to suppress warning.
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = a;
		return color;
	}

	color TransRGBA(color c, unsigned char a)
	{
		c.a = a / 255.0f;
		return c;
	}

	color TransRGBAf(color c, float a)
	{
		c.a = a;
		return c;
	}

	color LerpRGBA(color c0, color c1, float u)
	{
		color cint(0, 0, 0, 0);
		
		u = clampf(u, 0.0f, 1.0f);
		float oneminu = 1.0f - u;
		for(int i = 0; i <4; i++ )
		{
			cint.rgba[i] = c0.rgba[i] * oneminu + c1.rgba[i] * u;
		}

		return cint;
	}

	color HSL(float h, float s, float l)
	{
		return HSLA(h,s,l,255);
	}

	float hue(float h, float m1, float m2)
	{
		if (h < 0) h += 1;
		if (h > 1) h -= 1;
		if (h < 1.0f/6.0f)
			return m1 + (m2 - m1) * h * 6.0f;
		if (h < 3.0f/6.0f)
			return m2;
		if (h < 4.0f/6.0f)
			return m1 + (m2 - m1) * (2.0f/3.0f - h) * 6.0f;
		return m1;
	}

	color HSLA(float h, float s, float l, unsigned char a)
	{
		h = modf(h, 1.0f);
		if (h < 0.0f) h += 1.0f;
		s = clampf(s, 0.0f, 1.0f);
		l = clampf(l, 0.0f, 1.0f);
		float m2 = l <= 0.5f ? (l * (1 + s)) : (l + s - l * s);
		float m1 = 2 * l - m2;
		color col;
		col.r = clampf(hue(h + 1.0f/3.0f, m1, m2), 0.0f, 1.0f);
		col.g = clampf(hue(h, m1, m2), 0.0f, 1.0f);
		col.b = clampf(hue(h - 1.0f/3.0f, m1, m2), 0.0f, 1.0f);
		col.a = a/255.0f;
		return col;
	}

	void TransformIdentity(float* t)
	{
		t[0] = 1.0f; t[1] = 0.0f;
		t[2] = 0.0f; t[3] = 1.0f;
		t[4] = 0.0f; t[5] = 0.0f;
	}

	void TransformTranslate(float* t, float tx, float ty)
	{
		t[0] = 1.0f; t[1] = 0.0f;
		t[2] = 0.0f; t[3] = 1.0f;
		t[4] = tx; t[5] = ty;
	}

	void TransformScale(float* t, float sx, float sy)
	{
		t[0] = sx; t[1] = 0.0f;
		t[2] = 0.0f; t[3] = sy;
		t[4] = 0.0f; t[5] = 0.0f;
	}

	void TransformRotate(float* t, float a)
	{
		float cs = cosf(a), sn = sinf(a);
		t[0] = cs; t[1] = sn;
		t[2] = -sn; t[3] = cs;
		t[4] = 0.0f; t[5] = 0.0f;
	}

	void TransformSkewX(float* t, float a)
	{
		t[0] = 1.0f; t[1] = 0.0f;
		t[2] = tanf(a); t[3] = 1.0f;
		t[4] = 0.0f; t[5] = 0.0f;
	}

	void TransformSkewY(float* t, float a)
	{
		t[0] = 1.0f; t[1] = tanf(a);
		t[2] = 0.0f; t[3] = 1.0f;
		t[4] = 0.0f; t[5] = 0.0f;
	}

	void TransformMultiply(float* t, const float* s)
	{
		float t0 = t[0] * s[0] + t[1] * s[2];
		float t2 = t[2] * s[0] + t[3] * s[2];
		float t4 = t[4] * s[0] + t[5] * s[2] + s[4];
		t[1] = t[0] * s[1] + t[1] * s[3];
		t[3] = t[2] * s[1] + t[3] * s[3];
		t[5] = t[4] * s[1] + t[5] * s[3] + s[5];
		t[0] = t0;
		t[2] = t2;
		t[4] = t4;
	}

	void TransformPremultiply(float* t, const float* s)
	{
		float s2[6];
		memcpy(s2, s, sizeof(float)*6);
		TransformMultiply(s2, t);
		memcpy(t, s2, sizeof(float)*6);
	}

	int TransformInverse(float* inv, const float* t)
	{
		double det = (double)t[0] * t[3] - (double)t[2] * t[1];
		if (det > -1e-6 && det < 1e-6) {
			TransformIdentity(inv);
			return 0;
		}
		double invdet = 1.0 / det;
		inv[0] = (float)(t[3] * invdet);
		inv[2] = (float)(-t[2] * invdet);
		inv[4] = (float)(((double)t[2] * t[5] - (double)t[3] * t[4]) * invdet);
		inv[1] = (float)(-t[1] * invdet);
		inv[3] = (float)(t[0] * invdet);
		inv[5] = (float)(((double)t[1] * t[4] - (double)t[0] * t[5]) * invdet);
		return 1;
	}

	void TransformPoint(float* dx, float* dy, const float* t, float sx, float sy)
	{
		*dx = sx*t[0] + sy*t[2] + t[4];
		*dy = sx*t[1] + sy*t[3] + t[5];
	}

	float DegToRad(float deg)
	{
		return deg / 180.0f * NVG_PI;
	}

	float RadToDeg(float rad)
	{
		return rad / NVG_PI * 180.0f;
	}

	// State setting
	void Context::ShapeAntiAlias(int enabled)
	{
		GetState()->shapeAntiAlias = enabled;
	}

	void Context::StrokeWidth(float width)
	{
		GetState()->strokeWidth = width;
	}

	void Context::MiterLimit(float limit)
	{
		GetState()->miterLimit = limit;
	}

	void Context::LineCap(int cap)
	{
		GetState()->lineCap = cap;
	}

	void Context::LineJoin(int join)
	{
		GetState()->lineJoin = join;
	}

	void Context::GlobalAlpha(float alpha)
	{
		GetState()->alpha = alpha;
	}

	void Context::Transform(float a, float b, float c, float d, float e, float f)
	{
		float t[6] = { a, b, c, d, e, f };
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::ResetTransform()
	{
		TransformIdentity(GetState()->xform);
	}

	void Context::Translate(float x, float y)
	{
		float t[6];
		TransformTranslate(t, x,y);
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::Rotate(float angle)
	{
		float t[6];
		TransformRotate(t, angle);
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::SkewX(float angle)
	{
		float t[6];
		TransformSkewX(t, angle);
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::SkewY(float angle)
	{
		float t[6];
		TransformSkewY(t, angle);
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::Scale(float x, float y)
	{
		float t[6];
		TransformScale(t, x,y);
		TransformPremultiply(GetState()->xform, t);
	}

	void Context::CurrentTransform(float* xform)
	{
		if (!xform) return;
		memcpy(xform, GetState()->xform, sizeof(float) * 6);
	}

	void Context::StrokeColor(color color)
	{
		GetState()->stroke.SetColor(color);
	}

	void Context::StrokePaint(paint paint)
	{
		State* state = GetState();
		state->stroke = paint;
		TransformMultiply(state->stroke.xform, state->xform);
	}

	void Context::FillColor(color color)
	{
		GetState()->fill.SetColor(color);
	}

	void Context::FillPaint(paint paint)
	{
		State* state = GetState();
		state->fill = paint;
		TransformMultiply(state->fill.xform, state->xform);
	}

	int Context::CreateImage(const char* filename, int imageFlags)
	{
		stbi_set_unpremultiply_on_load(1);
		stbi_convert_iphone_png_to_rgb(1);
		int w, h, n;
		u8* img = stbi_load(filename, &w, &h, &n, 4);
		if (!img) return 0;
		int image = CreateImageRGBA(w, h, imageFlags, img);
		stbi_image_free(img);
		return image;
	}

	int Context::CreateImageMem(int imageFlags, unsigned char* data, int ndata)
	{
		int w, h, n;
		unsigned char* img = stbi_load_from_memory(data, ndata, &w, &h, &n, 4);
		if (!img) return 0;
		int image = CreateImageRGBA(w, h, imageFlags, img);
		stbi_image_free(img);
		return image;
	}

	int Context::CreateImageRGBA(int w, int h, int imageFlags, const unsigned char* data)
	{
		return glContext->CreateTexture(NVG_TEXTURE_RGBA, w, h, imageFlags, data);
	}

	void Context::UpdateImage(int image, const unsigned char* data)
	{
		auto extent = glContext->GetTextureSize(image);
		glContext->UpdateTexture(image, 0,0, extent->width, extent->height, data);
	}

	void Context::ImageSize(int image, int* w, int* h)
	{
		auto extent = glContext->GetTextureSize(image);
		*w = extent->width;
		*h = extent->height;
	}

	void Context::DeleteImage(int image)
	{
		glContext->DeleteTexture(image);
	}

	paint::paint()
	{
		memset(this, 0, sizeof(*this));
	}

	paint Context::LinearGradient(float sx, float sy, float ex, float ey, color icol, color ocol)
	{
		// Calculate transform aligned to the line
		float dx = ex - sx;
		float dy = ey - sy;
		float d = sqrtf(dx*dx + dy*dy);
		if (d > 0.0001f) 
		{
			dx /= d;
			dy /= d;
		} else {
			dx = 0;
			dy = 1;
		}

		const float large = 1e5;
		paint p;
		p.xform[0] = dy; p.xform[1] = -dx;
		p.xform[2] = dx; p.xform[3] = dy;
		p.xform[4] = sx - dx*large; p.xform[5] = sy - dy*large;

		p.extent[0] = large;
		p.extent[1] = large + d*0.5f;

		p.radius = 0.0f;

		p.feather = maxf(1.0f, d);

		p.innerColor = icol;
		p.outerColor = ocol;

		return p;
	}

	paint Context::RadialGradient(float cx, float cy, float inr, float outr, color icol, color ocol)
	{
		paint p;
		float r = (inr+outr)*0.5f;
		float f = (outr-inr);

		TransformIdentity(p.xform);
		p.xform[4] = cx;
		p.xform[5] = cy;

		p.extent[0] = r;
		p.extent[1] = r;

		p.radius = r;

		p.feather = maxf(1.0f, f);

		p.innerColor = icol;
		p.outerColor = ocol;

		return p;
	}

	paint Context::BoxGradient(float x, float y, float w, float h, float r, float f, color icol, color ocol)
	{
		paint p;

		TransformIdentity(p.xform);
		p.xform[4] = x+w*0.5f;
		p.xform[5] = y+h*0.5f;

		p.extent[0] = w*0.5f;
		p.extent[1] = h*0.5f;

		p.radius = r;

		p.feather = maxf(1.0f, f);

		p.innerColor = icol;
		p.outerColor = ocol;

		return p;
	}


	paint Context::ImagePattern(float cx, float cy, float w, float h, float angle, int image, float alpha)
	{
		paint p;
		NVG_NOTUSED(this);
		memset(&p, 0, sizeof(p));

		TransformRotate(p.xform, angle);
		p.xform[4] = cx;
		p.xform[5] = cy;

		p.extent[0] = w;
		p.extent[1] = h;

		p.image = image;

		p.innerColor = p.outerColor = RGBAf(1,1,1,alpha);

		return p;
	}

	// Scissoring
	void Context::Scissor(float x, float y, float w, float h)
	{
		State* state = GetState();

		w = maxf(0.0f, w);
		h = maxf(0.0f, h);

		TransformIdentity(state->scissor.xform);
		state->scissor.xform[4] = x+w*0.5f;
		state->scissor.xform[5] = y+h*0.5f;
		TransformMultiply(state->scissor.xform, state->xform);

		state->scissor.extent[0] = w*0.5f;
		state->scissor.extent[1] = h*0.5f;
	}

	void isectRects(float* dst, float ax, float ay, float aw, float ah,
								float bx, float by, float bw, float bh)
	{
		float minx = maxf(ax, bx);
		float miny = maxf(ay, by);
		float maxx = minf(ax+aw, bx+bw);
		float maxy = minf(ay+ah, by+bh);
		dst[0] = minx;
		dst[1] = miny;
		dst[2] = maxf(0.0f, maxx - minx);
		dst[3] = maxf(0.0f, maxy - miny);
	}

	void Context::IntersectScissor(float x, float y, float w, float h)
	{
		State* state = GetState();
		// If no previous scissor has been set, set the scissor as current scissor.
		if (state->scissor.extent[0] < 0) 
		{
			Scissor(x, y, w, h);
			return;
		}

		// Transform the current scissor rect into current transform space.
		// If there is difference in rotation, this will be approximation.
		float pxform[6], invxorm[6];
		memcpy(pxform, state->scissor.xform, sizeof(float) * 6);
		float ex = state->scissor.extent[0];
		float ey = state->scissor.extent[1];
		TransformInverse(invxorm, state->xform);
		TransformMultiply(pxform, invxorm);
		float tex = ex*absf(pxform[0]) + ey*absf(pxform[2]);
		float tey = ex*absf(pxform[1]) + ey*absf(pxform[3]);

		// Intersect rects.
		float rect[4];
		isectRects(rect, pxform[4]-tex,pxform[5]-tey,tex*2,tey*2, x,y,w,h);

		Scissor(rect[0], rect[1], rect[2], rect[3]);
	}

	void Context::ResetScissor()
	{
		State* state = GetState();
		memset(state->scissor.xform, 0, sizeof(state->scissor.xform));
		state->scissor.extent[0] = -1.0f;
		state->scissor.extent[1] = -1.0f;
	}

	// Global composite operation.
	void Context::GlobalCompositeOperation(int op)
	{
		GetState()->compositeOperation = compositeOperationState(op);
	}

	void Context::GlobalCompositeBlendFunc(int sfactor, int dfactor)
	{
		GlobalCompositeBlendFuncSeparate(sfactor, dfactor, sfactor, dfactor);
	}

	void Context::GlobalCompositeBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha)
	{
		CompositeOperationState op;
		op.srcRGB = srcRGB;
		op.dstRGB = dstRGB;
		op.srcAlpha = srcAlpha;
		op.dstAlpha = dstAlpha;

		GetState()->compositeOperation = op;
	}

	int ptEquals(float x1, float y1, float x2, float y2, float tol)
	{
		float dx = x2 - x1;
		float dy = y2 - y1;
		return dx*dx + dy*dy < tol*tol;
	}

	float distPtSeg(float x, float y, float px, float py, float qx, float qy)
	{
		float pqx = qx-px;
		float pqy = qy-py;
		float dx = x-px;
		float dy = y-py;
		float d = pqx*pqx + pqy*pqy;
		float t = pqx*dx + pqy*dy;
		if (d > 0) t /= d;
		if (t < 0) t = 0;
		else if (t > 1) t = 1;
		dx = px + t*pqx - x;
		dy = py + t*pqy - y;
		return dx*dx + dy*dy;
	}

	void Context::AppendCommands(float* vals, int nvals)
	{
		if ((int)vals[0] != CLOSE && (int)vals[0] != WINDING)
		{
			commandx = vals[nvals - 2];
			commandy = vals[nvals - 1];
		}

		// transform commands
		int i = 0;
		const auto state = GetState();
		while (i < nvals)
		{
			int cmd = (int)vals[i];
			switch (cmd) 
			{
			case MOVETO:
				TransformPoint(&vals[i + 1], &vals[i + 2], state->xform, vals[i + 1], vals[i + 2]);
				i += 3;
				break;
			case LINETO:
				TransformPoint(&vals[i + 1], &vals[i + 2], state->xform, vals[i + 1], vals[i + 2]);
				i += 3;
				break;
			case BEZIERTO:
				TransformPoint(&vals[i + 1], &vals[i + 2], state->xform, vals[i + 1], vals[i + 2]);
				TransformPoint(&vals[i + 3], &vals[i + 4], state->xform, vals[i + 3], vals[i + 4]);
				TransformPoint(&vals[i + 5], &vals[i + 6], state->xform, vals[i + 5], vals[i + 6]);
				i += 7;
				break;
			case CLOSE:
				i++;
				break;
			case WINDING:
				i += 2;
				break;
			default:
				i++;
			}
		}

		for (i = 0; i < nvals; ++i)
			commands.push_back(vals[i]);
	}

	void PathCache::Clear()
	{
		points.erase(points.begin(), points.end());
		paths.erase(paths.begin(), paths.end());
	}

	Path* Context::GetLastPath()
	{
		if (cache->paths.empty()) return nullptr;
		return &cache->paths.back();
	}

	Path::Path(const int first, const int winding)
	{
		memset(this, 0, sizeof(Path));
		this->first = first;
		this->winding = winding;
	}

	void PathCache::AddPath(const int first, const int winding)
	{
		paths.push_back(Path(first, winding));
	}

	void Context::AddPath()
	{
		cache->AddPath(cache->points.size(), NVG_CCW);
	}

	point* Context::GetLastPoint()
	{
		if (cache->points.empty()) return nullptr;
		return &cache->points.back();
	}

	void addPoint(Context* ctx, float x, float y, int flags)
	{
		Path* path = ctx->GetLastPath();
		if (!path) return;

		if (path->count > 0 && ctx->cache->points.size() > 0) 
		{
			point* pt = ctx->GetLastPoint();
			if (ptEquals(pt->x,pt->y, x,y, ctx->distTol)) 
			{
				pt->flags |= flags;
				return;
			}
		}

		ctx->cache->points.push_back(point());
		auto& pt = ctx->cache->points.back();
		memset(&pt, 0, sizeof(pt));
		pt.x = x;
		pt.y = y;
		pt.flags = (u8)flags;

		path->count++;
	}

	void closePath(Context* ctx)
	{
		Path* path = ctx->GetLastPath();
		if (path == NULL) return;
		path->closed = 1;
	}

	void pathWinding(Context* ctx, int winding)
	{
		Path* path = ctx->GetLastPath();
		if (path == NULL) return;
		path->winding = winding;
	}

	float getAverageScale(float *t)
	{
		float sx = sqrtf(t[0]*t[0] + t[2]*t[2]);
		float sy = sqrtf(t[1]*t[1] + t[3]*t[3]);
		return (sx + sy) * 0.5f;
	}

	vertex* Context::AllocTempVerts(const int nverts)
	{
		cache->verts.reserve(nverts);
		return cache->verts.data();
	}

	float triarea2(float ax, float ay, float bx, float by, float cx, float cy)
	{
		float abx = bx - ax;
		float aby = by - ay;
		float acx = cx - ax;
		float acy = cy - ay;
		return acx*aby - abx*acy;
	}

	float polyArea(point* pts, int npts)
	{
		int i;
		float area = 0;
		for (i = 2; i < npts; i++) {
			point* a = &pts[0];
			point* b = &pts[i-1];
			point* c = &pts[i];
			area += triarea2(a->x,a->y, b->x,b->y, c->x,c->y);
		}
		return area * 0.5f;
	}

	void polyReverse(point* pts, int npts)
	{
		point tmp;
		int i = 0, j = npts-1;
		while (i < j) {
			tmp = pts[i];
			pts[i] = pts[j];
			pts[j] = tmp;
			i++;
			j--;
		}
	}


	void vset(vertex* vtx, float x, float y, float u, float v)
	{
		vtx->x = x;
		vtx->y = y;
		vtx->u = u;
		vtx->v = v;
	}

	void Context::TesselateBezier(float x1, float y1, float x2, float y2,
								  float x3, float y3, float x4, float y4,
								  int level, int type)
	{
		float x12, y12, x23, y23, x34, y34, x123, y123, x234, y234, x1234, y1234;
		float dx, dy, d2, d3;

		if (level > 10) return;

		x12 = (x1 + x2)*0.5f;
		y12 = (y1 + y2)*0.5f;
		x23 = (x2 + x3)*0.5f;
		y23 = (y2 + y3)*0.5f;
		x34 = (x3 + x4)*0.5f;
		y34 = (y3 + y4)*0.5f;
		x123 = (x12 + x23)*0.5f;
		y123 = (y12 + y23)*0.5f;

		dx = x4 - x1;
		dy = y4 - y1;
		d2 = absf(((x2 - x4) * dy - (y2 - y4) * dx));
		d3 = absf(((x3 - x4) * dy - (y3 - y4) * dx));

		if ((d2 + d3)*(d2 + d3) < tessTol * (dx*dx + dy*dy)) 
		{
			addPoint(this, x4, y4, type);
			return;
		}

		x234 = (x23 + x34)*0.5f;
		y234 = (y23 + y34)*0.5f;
		x1234 = (x123 + x234)*0.5f;
		y1234 = (y123 + y234)*0.5f;

		TesselateBezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1, 0);
		TesselateBezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1, type);
	}

	void Context::FlattenPaths()
	{
		if (!cache->paths.empty())
			return;

		// Flatten
		int i = 0;
		while (i < commands.size()) 
		{
			const auto cmd = static_cast<int>(commands[i]);
			switch (cmd) 
			{
			case MOVETO:
			{
				AddPath();
				auto p = &commands[i+1];
				addPoint(this, p[0], p[1], NVG_PT_CORNER);
				i += 3;
			} break;
			case LINETO:
			{
				auto p = &commands[i + 1];
				addPoint(this, p[0], p[1], NVG_PT_CORNER);
				i += 3;
			} break;
			case BEZIERTO:
			{	
				const auto last = GetLastPoint();
				if (last)
				{
					const auto cp1 = &commands[i + 1];
					const auto cp2 = &commands[i + 3];
					auto p = &commands[i + 5];
					TesselateBezier(last->x, last->y, cp1[0], cp1[1], cp2[0], cp2[1], p[0], p[1], 0, NVG_PT_CORNER);
				}
				i += 7;
			} break;
			case CLOSE:
				closePath(this);
				i++;
				break;
			case WINDING:
				pathWinding(this, (int)commands[i+1]);
				i += 2;
				break;
			default:
				i++;
			}
		}

		cache->bounds[0] = cache->bounds[1] = 1e6f;
		cache->bounds[2] = cache->bounds[3] = -1e6f;

		// Calculate the direction and length of line segments.
		for(auto& path : cache->paths)
		{
			const auto pts = &cache->points[path.first];

			// If the first and last points are the same, remove the last, mark as closed path.
			auto p0 = &pts[path.count-1];
			auto p1 = &pts[0];
			if (ptEquals(p0->x,p0->y, p1->x,p1->y, distTol)) 
			{
				path.count--;
				p0 = &pts[path.count-1];
				path.closed = 1;
			}

			// Enforce winding.
			if (path.count > 2) 
			{
				float area = polyArea(pts, path.count);
				if (path.winding == NVG_CCW && area < 0.0f)
					polyReverse(pts, path.count);
				if (path.winding == NVG_CW && area > 0.0f)
					polyReverse(pts, path.count);
			}

			for(i = 0; i < path.count; i++) 
			{
				// Calculate segment direction and length
				p0->dx = p1->x - p0->x;
				p0->dy = p1->y - p0->y;
				p0->len = normalize(&p0->dx, &p0->dy);
				// Update bounds
				cache->bounds[0] = minf(cache->bounds[0], p0->x);
				cache->bounds[1] = minf(cache->bounds[1], p0->y);
				cache->bounds[2] = maxf(cache->bounds[2], p0->x);
				cache->bounds[3] = maxf(cache->bounds[3], p0->y);
				// Advance
				p0 = p1++;
			}
		}
	}

	int curveDivs(float r, float arc, float tol)
	{
		float da = acosf(r / (r + tol)) * 2.0f;
		return maxi(2, (int)ceilf(arc / da));
	}

	void chooseBevel(int bevel, point* p0, point* p1, float w,
								float* x0, float* y0, float* x1, float* y1)
	{
		if (bevel) {
			*x0 = p1->x + p0->dy * w;
			*y0 = p1->y - p0->dx * w;
			*x1 = p1->x + p1->dy * w;
			*y1 = p1->y - p1->dx * w;
		} else {
			*x0 = p1->x + p1->dmx * w;
			*y0 = p1->y + p1->dmy * w;
			*x1 = p1->x + p1->dmx * w;
			*y1 = p1->y + p1->dmy * w;
		}
	}

	vertex* roundJoin(vertex* dst, point* p0, point* p1, float lw, float rw, float lu, float ru, int ncap, float fringe)
	{
		int i, n;
		float dlx0 = p0->dy;
		float dly0 = -p0->dx;
		float dlx1 = p1->dy;
		float dly1 = -p1->dx;

		if (p1->flags & NVG_PT_LEFT) {
			float lx0,ly0,lx1,ly1,a0,a1;
			chooseBevel(p1->flags & NVG_PR_INNERBEVEL, p0, p1, lw, &lx0,&ly0, &lx1,&ly1);
			a0 = atan2f(-dly0, -dlx0);
			a1 = atan2f(-dly1, -dlx1);
			if (a1 > a0) a1 -= NVG_PI*2;

			vset(dst, lx0, ly0, lu,1); dst++;
			vset(dst, p1->x - dlx0*rw, p1->y - dly0*rw, ru,1); dst++;

			n = clampi((int)ceilf(((a0 - a1) / NVG_PI) * ncap), 2, ncap);
			for (i = 0; i < n; i++) {
				float u = i/(float)(n-1);
				float a = a0 + u*(a1-a0);
				float rx = p1->x + cosf(a) * rw;
				float ry = p1->y + sinf(a) * rw;
				vset(dst, p1->x, p1->y, 0.5f,1); dst++;
				vset(dst, rx, ry, ru,1); dst++;
			}

			vset(dst, lx1, ly1, lu,1); dst++;
			vset(dst, p1->x - dlx1*rw, p1->y - dly1*rw, ru,1); dst++;

		} else {
			float rx0,ry0,rx1,ry1,a0,a1;
			chooseBevel(p1->flags & NVG_PR_INNERBEVEL, p0, p1, -rw, &rx0,&ry0, &rx1,&ry1);
			a0 = atan2f(dly0, dlx0);
			a1 = atan2f(dly1, dlx1);
			if (a1 < a0) a1 += NVG_PI*2;

			vset(dst, p1->x + dlx0*rw, p1->y + dly0*rw, lu,1); dst++;
			vset(dst, rx0, ry0, ru,1); dst++;

			n = clampi((int)ceilf(((a1 - a0) / NVG_PI) * ncap), 2, ncap);
			for (i = 0; i < n; i++) {
				float u = i/(float)(n-1);
				float a = a0 + u*(a1-a0);
				float lx = p1->x + cosf(a) * lw;
				float ly = p1->y + sinf(a) * lw;
				vset(dst, lx, ly, lu,1); dst++;
				vset(dst, p1->x, p1->y, 0.5f,1); dst++;
			}

			vset(dst, p1->x + dlx1*rw, p1->y + dly1*rw, lu,1); dst++;
			vset(dst, rx1, ry1, ru,1); dst++;

		}
		return dst;
	}

	vertex* bevelJoin(vertex* dst, point* p0, point* p1, float lw, float rw, float lu, float ru, float fringe)
	{
		float rx0,ry0,rx1,ry1;
		float lx0,ly0,lx1,ly1;
		float dlx0 = p0->dy;
		float dly0 = -p0->dx;
		float dlx1 = p1->dy;
		float dly1 = -p1->dx;
		NVG_NOTUSED(fringe);

		if (p1->flags & NVG_PT_LEFT) {
			chooseBevel(p1->flags & NVG_PR_INNERBEVEL, p0, p1, lw, &lx0,&ly0, &lx1,&ly1);

			vset(dst, lx0, ly0, lu,1); dst++;
			vset(dst, p1->x - dlx0*rw, p1->y - dly0*rw, ru,1); dst++;

			if (p1->flags & NVG_PT_BEVEL) {
				vset(dst, lx0, ly0, lu,1); dst++;
				vset(dst, p1->x - dlx0*rw, p1->y - dly0*rw, ru,1); dst++;

				vset(dst, lx1, ly1, lu,1); dst++;
				vset(dst, p1->x - dlx1*rw, p1->y - dly1*rw, ru,1); dst++;
			} else {
				rx0 = p1->x - p1->dmx * rw;
				ry0 = p1->y - p1->dmy * rw;

				vset(dst, p1->x, p1->y, 0.5f,1); dst++;
				vset(dst, p1->x - dlx0*rw, p1->y - dly0*rw, ru,1); dst++;

				vset(dst, rx0, ry0, ru,1); dst++;
				vset(dst, rx0, ry0, ru,1); dst++;

				vset(dst, p1->x, p1->y, 0.5f,1); dst++;
				vset(dst, p1->x - dlx1*rw, p1->y - dly1*rw, ru,1); dst++;
			}

			vset(dst, lx1, ly1, lu,1); dst++;
			vset(dst, p1->x - dlx1*rw, p1->y - dly1*rw, ru,1); dst++;

		} else {
			chooseBevel(p1->flags & NVG_PR_INNERBEVEL, p0, p1, -rw, &rx0,&ry0, &rx1,&ry1);

			vset(dst, p1->x + dlx0*lw, p1->y + dly0*lw, lu,1); dst++;
			vset(dst, rx0, ry0, ru,1); dst++;

			if (p1->flags & NVG_PT_BEVEL) {
				vset(dst, p1->x + dlx0*lw, p1->y + dly0*lw, lu,1); dst++;
				vset(dst, rx0, ry0, ru,1); dst++;

				vset(dst, p1->x + dlx1*lw, p1->y + dly1*lw, lu,1); dst++;
				vset(dst, rx1, ry1, ru,1); dst++;
			} else {
				lx0 = p1->x + p1->dmx * lw;
				ly0 = p1->y + p1->dmy * lw;

				vset(dst, p1->x + dlx0*lw, p1->y + dly0*lw, lu,1); dst++;
				vset(dst, p1->x, p1->y, 0.5f,1); dst++;

				vset(dst, lx0, ly0, lu,1); dst++;
				vset(dst, lx0, ly0, lu,1); dst++;

				vset(dst, p1->x + dlx1*lw, p1->y + dly1*lw, lu,1); dst++;
				vset(dst, p1->x, p1->y, 0.5f,1); dst++;
			}

			vset(dst, p1->x + dlx1*lw, p1->y + dly1*lw, lu,1); dst++;
			vset(dst, rx1, ry1, ru,1); dst++;
		}

		return dst;
	}

	vertex* buttCapStart(vertex* dst, point* p,
											   float dx, float dy, float w, float d, float aa)
	{
		float px = p->x - dx*d;
		float py = p->y - dy*d;
		float dlx = dy;
		float dly = -dx;
		vset(dst, px + dlx*w - dx*aa, py + dly*w - dy*aa, 0,0); dst++;
		vset(dst, px - dlx*w - dx*aa, py - dly*w - dy*aa, 1,0); dst++;
		vset(dst, px + dlx*w, py + dly*w, 0,1); dst++;
		vset(dst, px - dlx*w, py - dly*w, 1,1); dst++;
		return dst;
	}

	vertex* buttCapEnd(vertex* dst, point* p,
											   float dx, float dy, float w, float d, float aa)
	{
		float px = p->x + dx*d;
		float py = p->y + dy*d;
		float dlx = dy;
		float dly = -dx;
		vset(dst, px + dlx*w, py + dly*w, 0,1); dst++;
		vset(dst, px - dlx*w, py - dly*w, 1,1); dst++;
		vset(dst, px + dlx*w + dx*aa, py + dly*w + dy*aa, 0,0); dst++;
		vset(dst, px - dlx*w + dx*aa, py - dly*w + dy*aa, 1,0); dst++;
		return dst;
	}


	vertex* roundCapStart(vertex* dst, point* p,
												float dx, float dy, float w, int ncap, float aa)
	{
		int i;
		float px = p->x;
		float py = p->y;
		float dlx = dy;
		float dly = -dx;
		NVG_NOTUSED(aa);
		for (i = 0; i < ncap; i++) {
			float a = i/(float)(ncap-1)*NVG_PI;
			float ax = cosf(a) * w, ay = sinf(a) * w;
			vset(dst, px - dlx*ax - dx*ay, py - dly*ax - dy*ay, 0,1); dst++;
			vset(dst, px, py, 0.5f,1); dst++;
		}
		vset(dst, px + dlx*w, py + dly*w, 0,1); dst++;
		vset(dst, px - dlx*w, py - dly*w, 1,1); dst++;
		return dst;
	}

	vertex* roundCapEnd(vertex* dst, point* p,
											  float dx, float dy, float w, int ncap, float aa)
	{
		int i;
		float px = p->x;
		float py = p->y;
		float dlx = dy;
		float dly = -dx;
		NVG_NOTUSED(aa);
		vset(dst, px + dlx*w, py + dly*w, 0,1); dst++;
		vset(dst, px - dlx*w, py - dly*w, 1,1); dst++;
		for (i = 0; i < ncap; i++) {
			float a = i/(float)(ncap-1)*NVG_PI;
			float ax = cosf(a) * w, ay = sinf(a) * w;
			vset(dst, px, py, 0.5f,1); dst++;
			vset(dst, px - dlx*ax + dx*ay, py - dly*ax + dy*ay, 0,1); dst++;
		}
		return dst;
	}


	void calculateJoins(Context* ctx, float w, int lineJoin, float miterLimit)
	{
		PathCache* cache = ctx->cache;
		int i, j;
		float iw = 0.0f;

		if (w > 0.0f) iw = 1.0f / w;

		// Calculate which joins needs extra vertices to append, and gather vertex count.
		for (i = 0; i < cache->paths.size(); i++) {
			Path* path = &cache->paths[i];
			point* pts = &cache->points[path->first];
			point* p0 = &pts[path->count-1];
			point* p1 = &pts[0];
			int nleft = 0;

			path->nbevel = 0;

			for (j = 0; j < path->count; j++) {
				float dlx0, dly0, dlx1, dly1, dmr2, cross, limit;
				dlx0 = p0->dy;
				dly0 = -p0->dx;
				dlx1 = p1->dy;
				dly1 = -p1->dx;
				// Calculate extrusions
				p1->dmx = (dlx0 + dlx1) * 0.5f;
				p1->dmy = (dly0 + dly1) * 0.5f;
				dmr2 = p1->dmx*p1->dmx + p1->dmy*p1->dmy;
				if (dmr2 > 0.000001f) {
					float scale = 1.0f / dmr2;
					if (scale > 600.0f) {
						scale = 600.0f;
					}
					p1->dmx *= scale;
					p1->dmy *= scale;
				}

				// Clear flags, but keep the corner.
				p1->flags = (p1->flags & NVG_PT_CORNER) ? NVG_PT_CORNER : 0;

				// Keep track of left turns.
				cross = p1->dx * p0->dy - p0->dx * p1->dy;
				if (cross > 0.0f) {
					nleft++;
					p1->flags |= NVG_PT_LEFT;
				}

				// Calculate if we should use bevel or miter for inner join.
				limit = maxf(1.01f, minf(p0->len, p1->len) * iw);
				if ((dmr2 * limit*limit) < 1.0f)
					p1->flags |= NVG_PR_INNERBEVEL;

				// Check to see if the corner needs to be beveled.
				if (p1->flags & NVG_PT_CORNER) {
					if ((dmr2 * miterLimit*miterLimit) < 1.0f || lineJoin == NVG_BEVEL || lineJoin == NVG_ROUND) {
						p1->flags |= NVG_PT_BEVEL;
					}
				}

				if ((p1->flags & (NVG_PT_BEVEL | NVG_PR_INNERBEVEL)) != 0)
					path->nbevel++;

				p0 = p1++;
			}

			path->convex = (nleft == path->count) ? 1 : 0;
		}
	}


	int expandStroke(Context* ctx, float w, int lineCap, int lineJoin, float miterLimit)
	{
		PathCache* cache = ctx->cache;
		float aa = ctx->fringeWidth;
		int ncap = curveDivs(w, NVG_PI, ctx->tessTol);	// Calculate divisions per half circle.

		calculateJoins(ctx, w, lineJoin, miterLimit);

		// Calculate max vertex usage.
		int cverts = 0;
        for(const auto& path : cache->paths)
        {
            if (lineJoin == NVG_ROUND)
                cverts += (path.count + path.nbevel*(ncap + 2) + 1) * 2; // plus one for loop
            else
                cverts += (path.count + path.nbevel * 5 + 1) * 2; // plus one for loop
            
            if (path.closed == 0) 
            {
                if (lineCap == NVG_ROUND) 
                    cverts += (ncap * 2 + 2) * 2;
                else 
                    cverts += (3 + 3) * 2;
            }
        }

        auto verts = ctx->AllocTempVerts(cverts);
        for (auto& path : cache->paths)
        {
            point* pts = &cache->points[path.first];
            point* p0;
            point* p1;
            int s, e;
            float dx, dy;

            path.fill = 0;
            path.nfill = 0;

            // Calculate fringe or stroke
            const auto loop = (path.closed == 0) ? false : true;
            auto dst = verts;
            path.stroke = dst;

            if (loop) 
            {
                // Looping
                p0 = &pts[path.count - 1];
                p1 = &pts[0];
                s = 0;
                e = path.count;
            }
            else 
            {
                // Add cap
                p0 = &pts[0];
                p1 = &pts[1];
                s = 1;
                e = path.count - 1;

                // Add cap
                dx = p1->x - p0->x;
                dy = p1->y - p0->y;
                normalize(&dx, &dy);
                if (lineCap == NVG_BUTT)
                    dst = buttCapStart(dst, p0, dx, dy, w, -aa*0.5f, aa);
                else if (lineCap == NVG_SQUARE)
                    dst = buttCapStart(dst, p0, dx, dy, w, w - aa, aa);
                else if (lineCap == NVG_ROUND)
                    dst = roundCapStart(dst, p0, dx, dy, w, ncap, aa);
            }

            for (int i = s; i < e; ++i) 
            {
                if ((p1->flags & (NVG_PT_BEVEL | NVG_PR_INNERBEVEL)) != 0) 
                {
                    if (lineJoin == NVG_ROUND) 
                        dst = roundJoin(dst, p0, p1, w, w, 0, 1, ncap, aa);
                    else 
                        dst = bevelJoin(dst, p0, p1, w, w, 0, 1, aa);
                }
                else 
                {
                    vset(dst, p1->x + (p1->dmx * w), p1->y + (p1->dmy * w), 0, 1); dst++;
                    vset(dst, p1->x - (p1->dmx * w), p1->y - (p1->dmy * w), 1, 1); dst++;
                }
                p0 = p1++;
            }

            if (loop) 
            {
                // Loop it
                vset(dst, verts[0].x, verts[0].y, 0, 1); dst++;
                vset(dst, verts[1].x, verts[1].y, 1, 1); dst++;
            }
            else 
            {
                // Add cap
                dx = p1->x - p0->x;
                dy = p1->y - p0->y;
                normalize(&dx, &dy);
                if (lineCap == NVG_BUTT)
                    dst = buttCapEnd(dst, p1, dx, dy, w, -aa*0.5f, aa);
                else if (lineCap == NVG_SQUARE)
                    dst = buttCapEnd(dst, p1, dx, dy, w, w - aa, aa);
                else if (lineCap == NVG_ROUND)
                    dst = roundCapEnd(dst, p1, dx, dy, w, ncap, aa);
            }

            path.nstroke = (int)(dst - verts);

            verts = dst;
        }
		
		return 1;
	}

	int expandFill(Context* ctx, float w, int lineJoin, float miterLimit)
	{
		PathCache* cache = ctx->cache;
		vertex* dst;
		int convex, i, j;
		float aa = ctx->fringeWidth;
		int fringe = w > 0.0f;

		calculateJoins(ctx, w, lineJoin, miterLimit);

		// Calculate max vertex usage.
		int cverts = 0;
        for (const auto& path : cache->paths)
        {
			cverts += path.count + path.nbevel + 1;
			if (fringe)
				cverts += (path.count + path.nbevel*5 + 1) * 2; // plus one for loop
        }

		auto verts = ctx->AllocTempVerts(cverts);
		
		convex = cache->paths.size() == 1 && cache->paths[0].convex;

		for (i = 0; i < cache->paths.size(); i++) {
			Path* path = &cache->paths[i];
			point* pts = &cache->points[path->first];
			point* p0;
			point* p1;
			float rw, lw, woff;
			float ru, lu;

			// Calculate shape vertices.
			woff = 0.5f*aa;
			dst = verts;
			path->fill = dst;

			if (fringe) {
				// Looping
				p0 = &pts[path->count-1];
				p1 = &pts[0];
				for (j = 0; j < path->count; ++j) {
					if (p1->flags & NVG_PT_BEVEL) {
						float dlx0 = p0->dy;
						float dly0 = -p0->dx;
						float dlx1 = p1->dy;
						float dly1 = -p1->dx;
						if (p1->flags & NVG_PT_LEFT) {
							float lx = p1->x + p1->dmx * woff;
							float ly = p1->y + p1->dmy * woff;
							vset(dst, lx, ly, 0.5f,1); dst++;
						} else {
							float lx0 = p1->x + dlx0 * woff;
							float ly0 = p1->y + dly0 * woff;
							float lx1 = p1->x + dlx1 * woff;
							float ly1 = p1->y + dly1 * woff;
							vset(dst, lx0, ly0, 0.5f,1); dst++;
							vset(dst, lx1, ly1, 0.5f,1); dst++;
						}
					} else {
						vset(dst, p1->x + (p1->dmx * woff), p1->y + (p1->dmy * woff), 0.5f,1); dst++;
					}
					p0 = p1++;
				}
			} else {
				for (j = 0; j < path->count; ++j) {
					vset(dst, pts[j].x, pts[j].y, 0.5f,1);
					dst++;
				}
			}

			path->nfill = (int)(dst - verts);
			verts = dst;

			// Calculate fringe
			if (fringe) {
				lw = w + woff;
				rw = w - woff;
				lu = 0;
				ru = 1;
				dst = verts;
				path->stroke = dst;

				// Create only half a fringe for convex shapes so that
				// the shape can be rendered without stenciling.
				if (convex) {
					lw = woff;	// This should generate the same vertex as fill inset above.
					lu = 0.5f;	// Set outline fade at middle.
				}

				// Looping
				p0 = &pts[path->count-1];
				p1 = &pts[0];

				for (j = 0; j < path->count; ++j) {
					if ((p1->flags & (NVG_PT_BEVEL | NVG_PR_INNERBEVEL)) != 0) {
						dst = bevelJoin(dst, p0, p1, lw, rw, lu, ru, ctx->fringeWidth);
					} else {
						vset(dst, p1->x + (p1->dmx * lw), p1->y + (p1->dmy * lw), lu,1); dst++;
						vset(dst, p1->x - (p1->dmx * rw), p1->y - (p1->dmy * rw), ru,1); dst++;
					}
					p0 = p1++;
				}

				// Loop it
				vset(dst, verts[0].x, verts[0].y, lu,1); dst++;
				vset(dst, verts[1].x, verts[1].y, ru,1); dst++;

				path->nstroke = (int)(dst - verts);
				verts = dst;
			} else {
				path->stroke = NULL;
				path->nstroke = 0;
			}
		}

		return 1;
	}

	// Draw
	void Context::BeginPath()
	{
		commands.clear();
		cache->Clear();
	}

	void Context::MoveTo(float x, float y)
	{
		float vals[] = { MOVETO, x, y };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::LineTo(float x, float y)
	{
		float vals[] = { LINETO, x, y };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::BezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y)
	{
		float vals[] = { BEZIERTO, c1x, c1y, c2x, c2y, x, y };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::QuadTo(float cx, float cy, float x, float y)
	{
		float x0 = commandx;
		float y0 = commandy;
		float vals[] = { BEZIERTO,
			x0 + 2.0f/3.0f*(cx - x0), y0 + 2.0f/3.0f*(cy - y0),
			x + 2.0f/3.0f*(cx - x), y + 2.0f/3.0f*(cy - y),
			x, y };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::ArcTo(float x1, float y1, float x2, float y2, float radius)
	{
		float x0 = commandx;
		float y0 = commandy;
		float dx0,dy0, dx1,dy1, a, d, cx,cy, a0,a1;
		int dir;

		if (commands.empty()) return;

		// Handle degenerate cases.
		if (ptEquals(x0,y0, x1,y1, distTol) ||
			ptEquals(x1,y1, x2,y2, distTol) ||
			distPtSeg(x1,y1, x0,y0, x2,y2) < distTol * distTol ||
			radius < distTol) {
			LineTo(x1,y1);
			return;
		}

		// Calculate tangential circle to lines (x0,y0)-(x1,y1) and (x1,y1)-(x2,y2).
		dx0 = x0-x1;
		dy0 = y0-y1;
		dx1 = x2-x1;
		dy1 = y2-y1;
		normalize(&dx0,&dy0);
		normalize(&dx1,&dy1);
		a = acosf(dx0*dx1 + dy0*dy1);
		d = radius / tanf(a/2.0f);

	//	printf("a=%f° d=%f\n", a/NVG_PI*180.0f, d);

		if (d > 10000.0f) {
			LineTo(x1,y1);
			return;
		}

		if (cross(dx0,dy0, dx1,dy1) > 0.0f) {
			cx = x1 + dx0*d + dy0*radius;
			cy = y1 + dy0*d + -dx0*radius;
			a0 = atan2f(dx0, -dy0);
			a1 = atan2f(-dx1, dy1);
			dir = NVG_CW;
	//		printf("CW c=(%f, %f) a0=%f° a1=%f°\n", cx, cy, a0/NVG_PI*180.0f, a1/NVG_PI*180.0f);
		} else {
			cx = x1 + dx0*d + -dy0*radius;
			cy = y1 + dy0*d + dx0*radius;
			a0 = atan2f(-dx0, dy0);
			a1 = atan2f(dx1, -dy1);
			dir = NVG_CCW;
	//		printf("CCW c=(%f, %f) a0=%f° a1=%f°\n", cx, cy, a0/NVG_PI*180.0f, a1/NVG_PI*180.0f);
		}

		Arc(cx, cy, radius, a0, a1, dir);
	}

	void Context::ClosePath()
	{
		float vals[] = { CLOSE };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::PathWinding(int dir)
	{
		float vals[] = { WINDING, (float)dir };
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::Arc(float cx, float cy, float r, float a0, float a1, int dir)
	{
		// Clamp angles
		float da = a1 - a0;
		if (dir == NVG_CW) 
        {
			if (absf(da) >= NVG_PI * 2) 
				da = NVG_PI * 2;
			else 
				while (da < 0.0f) da += NVG_PI * 2;
		} 
	    else 
        {
			if (absf(da) >= NVG_PI * 2) 
				da = -NVG_PI * 2;
			else 
				while (da > 0.0f) da -= NVG_PI * 2;
		}

		// Split arc into max 90 degree segments.
		int ndivs = maxi(1, mini((int)(absf(da) / (NVG_PI*0.5f) + 0.5f), 5));
		float hda = (da / (float)ndivs) / 2.0f;
		float kappa = absf(4.0f / 3.0f * (1.0f - cosf(hda)) / sinf(hda));

		if (dir == NVG_CCW)
			kappa = -kappa;

		int nvals = 0;
		float vals[3 + 5*7 + 100];
		int move = !commands.empty() ? LINETO : MOVETO;
        vals[nvals++] = (float)move;
        vals[nvals++] = cx + cosf(a0) * r;
        vals[nvals++] = cy + sinf(a0) * r;
        float px = cx + cosf(a0) * r;
        float py = cy + sinf(a0) * r;
        float ptanx = -sinf(a0) * r * kappa;
        float ptany = cosf(a0) * r * kappa;
		for (int i = 1; i <= ndivs; ++i) 
        {
			float a = a0 + da * (i/(float)ndivs);
			float dx = cosf(a);
			float dy = sinf(a);
			float x = cx + dx * r;
            float y = cy + dy * r;
			float tanx = -dy * r * kappa;
            float tany = dx * r * kappa;

            vals[nvals++] = BEZIERTO;
            vals[nvals++] = px + ptanx;
            vals[nvals++] = py + ptany;
            vals[nvals++] = x - tanx;
            vals[nvals++] = y - tany;
            vals[nvals++] = x;
            vals[nvals++] = y;

			px = x;
			py = y;
			ptanx = tanx;
			ptany = tany;
		}

		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::CreateRect(float x, float y, float w, float h)
	{
		float vals[] = {
			MOVETO, x,     y,
			LINETO, x,     y + h,
			LINETO, x + w, y + h,
			LINETO, x + w, y,
			CLOSE
		};
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::RoundedRect(float x, float y, float w, float h, float r)
	{
		RoundedRectVarying(x, y, w, h, r, r, r, r);
	}

	void Context::RoundedRectVarying(float x, float y, float w, float h, float radTopLeft, float radTopRight, float radBottomRight, float radBottomLeft)
	{
		if(radTopLeft < 0.1f && radTopRight < 0.1f && radBottomRight < 0.1f && radBottomLeft < 0.1f) 
		{
			CreateRect(x, y, w, h);
			return;
		} 

		float halfw = absf(w)*0.5f;
		float halfh = absf(h)*0.5f;
		float rxBL = minf(radBottomLeft, halfw) * signf(w), ryBL = minf(radBottomLeft, halfh) * signf(h);
		float rxBR = minf(radBottomRight, halfw) * signf(w), ryBR = minf(radBottomRight, halfh) * signf(h);
		float rxTR = minf(radTopRight, halfw) * signf(w), ryTR = minf(radTopRight, halfh) * signf(h);
		float rxTL = minf(radTopLeft, halfw) * signf(w), ryTL = minf(radTopLeft, halfh) * signf(h);
		float vals[] = {
			MOVETO, x, y + ryTL,
			LINETO, x, y + h - ryBL,
			BEZIERTO, x, y + h - ryBL*(1 - NVG_KAPPA90), x + rxBL*(1 - NVG_KAPPA90), y + h, x + rxBL, y + h,
			LINETO, x + w - rxBR, y + h,
			BEZIERTO, x + w - rxBR*(1 - NVG_KAPPA90), y + h, x + w, y + h - ryBR*(1 - NVG_KAPPA90), x + w, y + h - ryBR,
			LINETO, x + w, y + ryTR,
			BEZIERTO, x + w, y + ryTR*(1 - NVG_KAPPA90), x + w - rxTR*(1 - NVG_KAPPA90), y, x + w - rxTR, y,
			LINETO, x + rxTL, y,
			BEZIERTO, x + rxTL*(1 - NVG_KAPPA90), y, x, y + ryTL*(1 - NVG_KAPPA90), x, y + ryTL,
			CLOSE
		};

		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::Ellipse(float cx, float cy, float rx, float ry)
	{
		float vals[] = {
			MOVETO, cx-rx, cy,
			BEZIERTO, cx-rx, cy+ry*NVG_KAPPA90, cx-rx*NVG_KAPPA90, cy+ry, cx, cy+ry,
			BEZIERTO, cx+rx*NVG_KAPPA90, cy+ry, cx+rx, cy+ry*NVG_KAPPA90, cx+rx, cy,
			BEZIERTO, cx+rx, cy-ry*NVG_KAPPA90, cx+rx*NVG_KAPPA90, cy-ry, cx, cy-ry,
			BEZIERTO, cx-rx*NVG_KAPPA90, cy-ry, cx-rx, cy-ry*NVG_KAPPA90, cx-rx, cy,
			CLOSE
		};
		AppendCommands(vals, NVG_COUNTOF(vals));
	}

	void Context::Circle(float cx, float cy, float r)
	{
		Ellipse(cx,cy, r,r);
	}

	void Context::DebugDumpPathCache()
	{
		const Path* path;
		int i, j;

		printf("Dumping %d cached paths\n", cache->paths.size());
		for (i = 0; i < cache->paths.size(); i++) {
			path = &cache->paths[i];
			printf(" - Path %d\n", i);
			if (path->nfill) {
				printf("   - fill: %d\n", path->nfill);
				for (j = 0; j < path->nfill; j++)
					printf("%f\t%f\n", path->fill[j].x, path->fill[j].y);
			}
			if (path->nstroke) {
				printf("   - stroke: %d\n", path->nstroke);
				for (j = 0; j < path->nstroke; j++)
					printf("%f\t%f\n", path->stroke[j].x, path->stroke[j].y);
			}
		}
	}

	void Context::Fill()
	{
		State* state = GetState();

		FlattenPaths();
		if (edgeAA && state->shapeAntiAlias)
			expandFill(this, fringeWidth, NVG_MITER, 2.4f);
		else
			expandFill(this, 0.0f, NVG_MITER, 2.4f);

		// Apply global alpha
		paint fillPaint = state->fill;
		fillPaint.innerColor.a *= state->alpha;
		fillPaint.outerColor.a *= state->alpha;
		glContext->RenderFill(state->compositeOperation, fillPaint, state->scissor, fringeWidth, cache->bounds, cache->paths.data(), cache->paths.size());

		// Count triangles
		for(const auto& path : cache->paths)
		{
			fillTriCount += path.nfill - 2;
			fillTriCount += path.nstroke - 2;
			drawCallCount += 2;
		}
	}

	void Context::Stroke()
	{
		State* state = GetState();
		
		paint strokePaint = state->stroke;
		float scale = getAverageScale(state->xform);
		float strokeWidth = clampf(state->strokeWidth * scale, 0.0f, 200.0f);
		if (strokeWidth < fringeWidth) 
		{
			// If the stroke width is less than pixel size, use alpha to emulate coverage.
			// Since coverage is area, scale by alpha*alpha.
			float alpha = clampf(strokeWidth / fringeWidth, 0.0f, 1.0f);
			strokePaint.innerColor.a *= alpha*alpha;
			strokePaint.outerColor.a *= alpha*alpha;
			strokeWidth = fringeWidth;
		}

		// Apply global alpha
		strokePaint.innerColor.a *= state->alpha;
		strokePaint.outerColor.a *= state->alpha;

		FlattenPaths();

		if (edgeAA && state->shapeAntiAlias)
			expandStroke(this, strokeWidth*0.5f + fringeWidth*0.5f, state->lineCap, state->lineJoin, state->miterLimit);
		else
			expandStroke(this, strokeWidth*0.5f, state->lineCap, state->lineJoin, state->miterLimit);

		glContext->RenderStroke(state->compositeOperation, strokePaint, state->scissor, fringeWidth, strokeWidth, cache->paths.data(), cache->paths.size());

		// Count triangles
		for(const auto& path : cache->paths)
		{
			strokeTriCount += path.nstroke - 2;
			drawCallCount++;
		}
	}

	// State setting
	void Context::SetFontSize(const float size)
	{
		GetState()->fontSize = size;
	}

	void Context::FontBlur(float blur)
	{
		GetState()->fontBlur = blur;
	}

	void Context::TextLetterSpacing(float spacing)
	{
		GetState()->letterSpacing = spacing;
	}

	void Context::TextLineHeight(float lineHeight)
	{
		GetState()->lineHeight = lineHeight;
	}

	void Context::TextAlign(int align)
	{
		GetState()->textAlign = align;
	}

	void Context::SetFontFace(const std::string& font)
	{
		GetState()->fontId = fs->GetFontByName(font);
	}

	void Context::SetFontFace(int font)
	{
		GetState()->fontId = font;
	}

	float quantize(float a, float d)
	{
		return ((int)(a / d + 0.5f)) * d;
	}

	float State::GetFontScale()
	{
		return minf(quantize(getAverageScale(xform), 0.01f), 4.0f);
	}

	void Context::FlushTextTexture()
	{
		int dirty[4];
		if (!fs->ValidateTexture(dirty)) return;

		int fontImage = fontImages[fontImageIdx];
		if (!fontImage) return;
		
		int iw, ih;
		auto data = fs->GetTextureData(&iw, &ih);
		int x = dirty[0];
		int y = dirty[1];
		int w = dirty[2] - dirty[0];
		int h = dirty[3] - dirty[1];
		glContext->UpdateTexture(fontImage, x, y, w, h, data);
	}

	bool Context::AllocTextAtlas()
	{
		FlushTextTexture();
		if (fontImageIdx >= NVG_MAX_FONTIMAGES - 1)
			return false;

		// if next fontImage already have a texture
		int iw, ih;
		if (fontImages[fontImageIdx + 1] != 0)
		{
			ImageSize(fontImages[fontImageIdx + 1], &iw, &ih);
		}
		else 
		{ // calculate the new font image size and create it.
			ImageSize(fontImages[fontImageIdx], &iw, &ih);
			if (iw > ih)
				ih *= 2;
			else
				iw *= 2;
			if (iw > NVG_MAX_FONTIMAGE_SIZE || ih > NVG_MAX_FONTIMAGE_SIZE)
				iw = ih = NVG_MAX_FONTIMAGE_SIZE;
			fontImages[fontImageIdx + 1] = glContext->CreateTexture(NVG_TEXTURE_ALPHA, iw, ih, 0, NULL);
		}
		++fontImageIdx;
		fs->ResetAtlas(iw, ih);
		return true;
	}

	void Context::RenderText(vertex* verts, int nverts)
	{
		State* state = GetState();
		paint paint = state->fill;

		// Render triangles.
		paint.image = fontImages[fontImageIdx];

		// Apply global alpha
		paint.innerColor.a *= state->alpha;
		paint.outerColor.a *= state->alpha;

		glContext->RenderTriangles(state->compositeOperation, paint, state->scissor, verts, nverts);

		drawCallCount++;
		textTriCount += nverts/3;
	}

	float Context::Text(float x, float y, const char* string, const char* end)
	{
		State* state = GetState();
		if (!state->IsFontValid()) return x;

		float scale = state->GetFontScale() * devicePxRatio;
		float invscale = 1.0f / scale;

		if (!end)
			end = string + strlen(string);

		fs->SetSize(state->fontSize*scale);
		fs->SetSpacing(state->letterSpacing*scale);
		fs->SetBlur(state->fontBlur*scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);

		int nverts = 0;
		int cverts = maxi(2, (int)(end - string)) * 6; // conservative estimate.
		vertex* verts = AllocTempVerts(cverts);
		if (!verts) return x;

		fons::TextIter iter = *fs->TextIterInit(x * scale, y * scale, string, end);
		fons::TextIter prevIter = iter;
		fons::Quad q;
		while (fs->TextIterNext(&iter, &q)) 
		{
			float c[4 * 2];
			if (iter.prevGlyphIndex == -1) 
			{ // can not retrieve glyph?
				if (!AllocTextAtlas())
					break; // no memory :(
				if (nverts != 0)
				{
					RenderText(verts, nverts);
					nverts = 0;
				}
				iter = prevIter;
				fs->TextIterNext(&iter, &q); // try again
				if (iter.prevGlyphIndex == -1) // still can not find glyph?
					break;
			}
			prevIter = iter;
			// Transform corners.
			TransformPoint(&c[0], &c[1], state->xform, q.x0*invscale, q.y0*invscale);
			TransformPoint(&c[2], &c[3], state->xform, q.x1*invscale, q.y0*invscale);
			TransformPoint(&c[4], &c[5], state->xform, q.x1*invscale, q.y1*invscale);
			TransformPoint(&c[6], &c[7], state->xform, q.x0*invscale, q.y1*invscale);
			// Create triangles
			if (nverts + 6 <= cverts)
			{
				vset(&verts[nverts], c[0], c[1], q.s0, q.t0); nverts++;
				vset(&verts[nverts], c[4], c[5], q.s1, q.t1); nverts++;
				vset(&verts[nverts], c[2], c[3], q.s1, q.t0); nverts++;
				vset(&verts[nverts], c[0], c[1], q.s0, q.t0); nverts++;
				vset(&verts[nverts], c[6], c[7], q.s0, q.t1); nverts++;
				vset(&verts[nverts], c[4], c[5], q.s1, q.t1); nverts++;
			}
		}

		// TODO: add back-end bit to do this just once per frame.
		FlushTextTexture();

		RenderText(verts, nverts);

		return iter.nextx / scale;
	}

	void Context::TextBox(float x, float y, float breakRowWidth, const char* string, const char* end)
	{
		const auto state = GetState();
		if (!state->IsFontValid()) return;

		int oldAlign = state->textAlign;
		int haling = state->textAlign & (LEFT | CENTER | RIGHT);
		int valign = state->textAlign & (TOP | MIDDLE | BOTTOM | BASELINE);
		state->textAlign = LEFT | valign;

		textRow rows[2];
		const float lineh = ComputeTextMetrics().lineh;
		for(;;)
		{
			int nrows = TextBreakLines(string, end, breakRowWidth, rows, 2);
			if (!nrows)
				break;

			for (int i = 0; i < nrows; i++) 
			{
				textRow* row = &rows[i];
				if (haling & LEFT)
					Text(x, y, row->start, row->end);
				else if (haling & CENTER)
					Text(x + breakRowWidth*0.5f - row->width*0.5f, y, row->start, row->end);
				else if (haling & RIGHT)
					Text(x + breakRowWidth - row->width, y, row->start, row->end);
				y += lineh * state->lineHeight;
			}
			string = rows[nrows-1].next;
		}

		state->textAlign = oldAlign;
	}

	std::optional<std::vector<glyphPosition>> Context::TextGlyphPositions(float x, float y, const char* string, const char* end, int maxPositions)
	{
		State* state = GetState();
		if (!state->IsFontValid()) return {};

		if (!end)
			end = string + strlen(string);
		if (string == end) return {};

		float scale = state->GetFontScale() * devicePxRatio;
		float invscale = 1.0f / scale;
		fs->SetSize(state->fontSize*scale);
		fs->SetSpacing(state->letterSpacing*scale);
		fs->SetBlur(state->fontBlur*scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);

		fons::TextIter iter = *fs->TextIterInit(x * scale, y * scale, string, end);
		fons::TextIter prevIter = iter;
		std::vector<glyphPosition> positions(maxPositions); 
		fons::Quad q;

		int npos = 0;
		while (fs->TextIterNext(&iter, &q))
		{
			if (iter.prevGlyphIndex < 0 && AllocTextAtlas()) 
			{ // can not retrieve glyph?
				iter = prevIter;
				fs->TextIterNext(&iter, &q); // try again
			}
			prevIter = iter;
			positions[npos].str = iter.str;
			positions[npos].x = iter.x * invscale;
			positions[npos].minx = minf(iter.x, q.x0) * invscale;
			positions[npos].maxx = maxf(iter.nextx, q.x1) * invscale;
			npos++;
			if (npos >= maxPositions)
				break;
		}

		return positions;
	}

	enum CodepointType 
	{
		SPACE,
		NEWLINE,
		CHAR,
		CJK_CHAR,
	};

	int Context::TextBreakLines(const char* string, const char* end, float breakRowWidth, textRow* rows, int maxRows)
	{
		State* state = GetState();
		if (!state->IsFontValid()) return 0;

		if (maxRows == 0) return 0;

		int nrows = 0;
		float rowStartX = 0;
		float rowWidth = 0;
		float rowMinX = 0;
		float rowMaxX = 0;
		const char* rowStart = NULL;
		const char* rowEnd = NULL;
		const char* wordStart = NULL;
		float wordStartX = 0;
		float wordMinX = 0;
		const char* breakEnd = NULL;
		float breakWidth = 0;
		float breakMaxX = 0;
		int type = SPACE, ptype = SPACE;
		unsigned int pcodepoint = 0;

		if (!end)
			end = string + strlen(string);

		if (string == end) return 0;

		float scale = state->GetFontScale() * devicePxRatio;
		float invscale = 1.0f / scale;
		fs->SetSize(state->fontSize*scale);
		fs->SetSpacing(state->letterSpacing*scale);
		fs->SetBlur(state->fontBlur*scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);

		breakRowWidth *= scale;

		fons::TextIter iter = *fs->TextIterInit(0, 0, string, end);
		fons::TextIter prevIter = iter;
		fons::Quad q;

		while (fs->TextIterNext(&iter, &q))
		{
			if (iter.prevGlyphIndex < 0 && AllocTextAtlas()) 
			{ // can not retrieve glyph?
				iter = prevIter;
				fs->TextIterNext(&iter, &q); // Try again
			}
			prevIter = iter;
			switch (iter.codepoint) 
			{
				case 9:			// \t
				case 11:		// \v
				case 12:		// \f
				case 32:		// space
				case 0x00a0:	// NBSP
					type = SPACE;
					break;
				case 10:		// \n
					type = pcodepoint == 13 ? SPACE : NEWLINE;
					break;
				case 13:		// \r
					type = pcodepoint == 10 ? SPACE : NEWLINE;
					break;
				case 0x0085:	// NEL
					type = NEWLINE;
					break;
				default:
					if ((iter.codepoint >= 0x4E00 && iter.codepoint <= 0x9FFF) ||
						(iter.codepoint >= 0x3000 && iter.codepoint <= 0x30FF) ||
						(iter.codepoint >= 0xFF00 && iter.codepoint <= 0xFFEF) ||
						(iter.codepoint >= 0x1100 && iter.codepoint <= 0x11FF) ||
						(iter.codepoint >= 0x3130 && iter.codepoint <= 0x318F) ||
						(iter.codepoint >= 0xAC00 && iter.codepoint <= 0xD7AF))
						type = CJK_CHAR;
					else
						type = CHAR;
					break;
			}

			if (type == NEWLINE) 
			{
				// Always handle new lines.
				rows[nrows].start = rowStart != NULL ? rowStart : iter.str;
				rows[nrows].end = rowEnd != NULL ? rowEnd : iter.str;
				rows[nrows].width = rowWidth * invscale;
				rows[nrows].minx = rowMinX * invscale;
				rows[nrows].maxx = rowMaxX * invscale;
				rows[nrows].next = iter.next;
				nrows++;
				if (nrows >= maxRows)
					return nrows;
				// Set null break point
				breakEnd = rowStart;
				breakWidth = 0.0;
				breakMaxX = 0.0;
				// Indicate to skip the white space at the beginning of the row.
				rowStart = NULL;
				rowEnd = NULL;
				rowWidth = 0;
				rowMinX = rowMaxX = 0;
			} else 
			{
				if (!rowStart) 
				{
					// Skip white space until the beginning of the line
					if (type == CHAR || type == CJK_CHAR) 
					{
						// The current char is the row so far
						rowStartX = iter.x;
						rowStart = iter.str;
						rowEnd = iter.next;
						rowWidth = iter.nextx - rowStartX; // q.x1 - rowStartX;
						rowMinX = q.x0 - rowStartX;
						rowMaxX = q.x1 - rowStartX;
						wordStart = iter.str;
						wordStartX = iter.x;
						wordMinX = q.x0 - rowStartX;
						// Set null break point
						breakEnd = rowStart;
						breakWidth = 0.0;
						breakMaxX = 0.0;
					}
				} 
				else 
				{
					float nextWidth = iter.nextx - rowStartX;

					// track last non-white space character
					if (type == CHAR || type == CJK_CHAR) 
					{
						rowEnd = iter.next;
						rowWidth = iter.nextx - rowStartX;
						rowMaxX = q.x1 - rowStartX;
					}
					// track last end of a word
					if (((ptype == CHAR || ptype == CJK_CHAR) && type == SPACE) || type == CJK_CHAR) 
					{
						breakEnd = iter.str;
						breakWidth = rowWidth;
						breakMaxX = rowMaxX;
					}
					// track last beginning of a word
					if ((ptype == SPACE && (type == CHAR || type == CJK_CHAR)) || type == CJK_CHAR) 
					{
						wordStart = iter.str;
						wordStartX = iter.x;
						wordMinX = q.x0 - rowStartX;
					}

					// Break to new line when a character is beyond break width.
					if ((type == CHAR || type == CJK_CHAR) && nextWidth > breakRowWidth) 
					{
						// The run length is too long, need to break to new line.
						if (breakEnd == rowStart) 
						{
							// The current word is longer than the row length, just break it from here.
							rows[nrows].start = rowStart;
							rows[nrows].end = iter.str;
							rows[nrows].width = rowWidth * invscale;
							rows[nrows].minx = rowMinX * invscale;
							rows[nrows].maxx = rowMaxX * invscale;
							rows[nrows].next = iter.str;
							nrows++;
							if (nrows >= maxRows)
								return nrows;
							rowStartX = iter.x;
							rowStart = iter.str;
							rowEnd = iter.next;
							rowWidth = iter.nextx - rowStartX;
							rowMinX = q.x0 - rowStartX;
							rowMaxX = q.x1 - rowStartX;
							wordStart = iter.str;
							wordStartX = iter.x;
							wordMinX = q.x0 - rowStartX;
						} 
						else 
						{
							// Break the line from the end of the last word, and start new line from the beginning of the new.
							rows[nrows].start = rowStart;
							rows[nrows].end = breakEnd;
							rows[nrows].width = breakWidth * invscale;
							rows[nrows].minx = rowMinX * invscale;
							rows[nrows].maxx = breakMaxX * invscale;
							rows[nrows].next = wordStart;
							nrows++;
							if (nrows >= maxRows)
								return nrows;
							rowStartX = wordStartX;
							rowStart = wordStart;
							rowEnd = iter.next;
							rowWidth = iter.nextx - rowStartX;
							rowMinX = wordMinX;
							rowMaxX = q.x1 - rowStartX;
							// No change to the word start
						}
						// Set null break point
						breakEnd = rowStart;
						breakWidth = 0.0;
						breakMaxX = 0.0;
					}
				}
			}

			pcodepoint = iter.codepoint;
			ptype = type;
		}

		// Break the line from the end of the last word, and start new line from the beginning of the new.
		if (rowStart) 
		{
			rows[nrows].start = rowStart;
			rows[nrows].end = rowEnd;
			rows[nrows].width = rowWidth * invscale;
			rows[nrows].minx = rowMinX * invscale;
			rows[nrows].maxx = rowMaxX * invscale;
			rows[nrows].next = end;
			nrows++;
		}

		return nrows;
	}

	std::optional<Context::TextBoundsResult> Context::TextBounds(float x, float y, const char* string, const char* end)
	{
		State* state = GetState();
		if (!state->IsFontValid()) return {};

		const float scale = state->GetFontScale() * devicePxRatio;
		fs->SetSize(state->fontSize * scale);
		fs->SetSpacing(state->letterSpacing * scale);
		fs->SetBlur(state->fontBlur * scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);

		TextBoundsResult result;

		// Use line bounds for height.
		const float invscale = 1.0f / scale;
		fs->LineBounds(y * scale, &result.bounds.ymin, &result.bounds.ymax);
		result.bounds.ScaleUniform(invscale);

		result.advanceX = fs->TextBounds(x * scale, y * scale, string, end, result.bounds.coord) * invscale;

		return result;
	}

	std::optional<Rect> Context::TextBoxBounds(float x, float y, float breakRowWidth, const char* string, const char* end)
	{
		const auto state = GetState();
		if (!state->IsFontValid()) return {};
		
		textRow rows[2];
		int oldAlign = state->textAlign;
		int haling = state->textAlign & (LEFT | CENTER | RIGHT);
		int valign = state->textAlign & (TOP | MIDDLE | BOTTOM | BASELINE);

		state->textAlign = LEFT | valign;

		float minx = x;
		float maxx = x;
		float miny = y;
		float maxy = y;

		float scale = state->GetFontScale() * devicePxRatio;
		float invscale = 1.0f / scale;
		fs->SetSize(state->fontSize*scale);
		fs->SetSpacing(state->letterSpacing*scale);
		fs->SetBlur(state->fontBlur*scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);
		float rminy = 0, rmaxy = 0;
		fs->LineBounds(0, &rminy, &rmaxy);
		rminy *= invscale;
		rmaxy *= invscale;

		const float lineh = ComputeTextMetrics().lineh;
		for(;;)
		{
			int nrows = TextBreakLines(string, end, breakRowWidth, rows, 2);
			if (!nrows) break;

			for (int i = 0; i < nrows; i++)
			{
				textRow* row = &rows[i];
				float dx = 0;

				// Horizontal bounds
				if (haling & LEFT)
					dx = 0;
				else if (haling & CENTER)
					dx = breakRowWidth*0.5f - row->width*0.5f;
				else if (haling & RIGHT)
					dx = breakRowWidth - row->width;
				const float rminx = x + row->minx + dx;
				const float rmaxx = x + row->maxx + dx;
				minx = minf(minx, rminx);
				maxx = maxf(maxx, rmaxx);

				// Vertical bounds.
				miny = minf(miny, y + rminy);
				maxy = maxf(maxy, y + rmaxy);

				y += lineh * state->lineHeight;
			}
			string = rows[nrows - 1].next;
		}

		state->textAlign = oldAlign;

		return Rect(minx, miny, maxx, maxy);
	}

	bool State::IsFontValid() const
	{
		return (fontId != FONS_INVALID);
	}

	fons::TextMetrics Context::ComputeTextMetrics()
	{
		const auto state = GetState();

		const float scale = state->GetFontScale() * devicePxRatio;
		fs->SetSize(state->fontSize * scale);
		fs->SetSpacing(state->letterSpacing * scale);
		fs->SetBlur(state->fontBlur * scale);
		fs->SetAlign(state->textAlign);
		fs->SetFont(state->fontId);

		auto metrics = fs->VertMetrics();
		metrics.UniformScale(1.f / scale);
		return metrics;
	}
}

Rect::Rect() 
	: xmin(0.f), ymin(0.f)
	, xmax(0.f), ymax(0.f)
{
}

Rect::Rect(float xmin, float ymin, float xmax, float ymax)
	: xmin(xmin), ymin(ymin)
	, xmax(xmax), ymax(ymax)
{
}

void Rect::ScaleUniform(const float scale)
{
	xmin *= scale;
	ymin *= scale;
	xmax *= scale;
	ymax *= scale;
}
