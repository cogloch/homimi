#include "stdafx.h"
#include "ui.h"
#include <nanovg\nanovg.h>
#include "input.h"
#include <glm\glm.hpp>
#include <nanovg/nanovg.h>
#include <cassert>


InputState* UIManager::input = nullptr;


std::string cpToUTF8(int cp)
{
	int n = 0;
	if (cp < 0x80)			   n = 1;
	else if (cp < 0x800)       n = 2;
	else if (cp < 0x10000)     n = 3;
	else if (cp < 0x200000)    n = 4;
	else if (cp < 0x4000000)   n = 5;
	else if (cp <= 0x7fffffff) n = 6;
	
	std::string str;
	str.resize(n);
	switch (n) 
	{
		case 6: str[5] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x4000000;
		case 5: str[4] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x200000;
		case 4: str[3] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x10000;
		case 3: str[2] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0x800;
		case 2: str[1] = 0x80 | (cp & 0x3f); cp = cp >> 6; cp |= 0xc0;
		case 1: str[0] = cp;
	}
	return str;
}

static float maxf(float a, float b) { return a > b ? a : b; }
static float clampf(float a, float mn, float mx) { return a < mn ? mn : (a > mx ? mx : a); }

// Returns 1 if col.rgba is 0.0f,0.0f,0.0f,0.0f, 0 otherwise
bool IsBlack(const Color col)
{
	return (col.r == 0.0f && col.g == 0.0f && col.b == 0.0f && col.a == 0.0f);
}

void Label::Render(nvg::Context& vg) const
{
	vg.SetFontSize(fontSize);
	vg.SetFontFace(fontFace);
	vg.FillColor({ color.r, color.g, color.b, color.a });
	vg.TextAlign(align);
	vg.Text(x, y + height * .5f, text.c_str(), NULL);
}

Color::Color(u8 r, u8 g, u8 b, u8 a)
	: r(r / 255.0f)
	, g(g / 255.0f)
	, b(b / 255.0f)
	, a(a / 255.0f)
{
}

//void Screen::Render(nvg::Context& vg)
//{
//	for (const auto& elem : elements)
//	{
//		switch (elem.type)
//		{
//		case UIElement::Type::LABEL:
//			labels[elem.idx].Render(vg);
//			break;
//		}
//	}
//}
//
//void Screen::AddLabel(Label label)
//{
//	elements.push_back({ UIElement::Type::LABEL, labels.size() });
//	labels.push_back(std::move(label));
//}
//
//void Screen::AddWindow(Window window)
//{
//	elements.push_back({ UIElement::Type::WINDOW, windows.size() });
//	windows.push_back(std::move(window));
//}

void Window::Render(nvg::Context& vg) const
{
	float cornerRadius = 3.0f;

	vg.Save();

	// Window
	vg.BeginPath();
	vg.RoundedRect(x, y, w, h, cornerRadius);
	vg.FillColor(nvg::RGBA(28, 30, 34, 192));
	//	vg.FillColor(nvg::RGBA(0,0,0,128));
	vg.Fill();

	// Drop shadow
	nvg::paint shadowPaint = vg.BoxGradient(x, y + 2, w, h, cornerRadius * 2, 10, nvg::RGBA(0, 0, 0, 128), nvg::RGBA(0, 0, 0, 0));
	vg.BeginPath();
	vg.CreateRect(x - 10, y - 10, w + 20, h + 30);
	vg.RoundedRect(x, y, w, h, cornerRadius);
	vg.PathWinding(nvg::NVG_HOLE);
	vg.FillPaint(shadowPaint);
	vg.Fill();

	// Header
	nvg::paint headerPaint = vg.LinearGradient(x, y, x, y + 15, nvg::RGBA(255, 255, 255, 8), nvg::RGBA(0, 0, 0, 16));
	vg.BeginPath();
	vg.RoundedRect(x + 1, y + 1, w - 2, 30, cornerRadius - 1);
	vg.FillPaint(headerPaint);
	vg.Fill();
	vg.BeginPath();
	vg.MoveTo(x + 0.5f, y + 0.5f + 30);
	vg.LineTo(x + 0.5f + w - 1, y + 0.5f + 30);
	vg.StrokeColor(nvg::RGBA(0, 0, 0, 32));
	vg.Stroke();

	vg.SetFontSize(18.0f);
	vg.SetFontFace("sans-bold");
	vg.TextAlign(nvg::Align::CENTER | nvg::Align::MIDDLE);

	vg.FontBlur(2);
	vg.FillColor(nvg::RGBA(0, 0, 0, 128));
	vg.Text(x + w / 2, y + 16 + 1, title.c_str(), NULL);

	vg.FontBlur(0);
	vg.FillColor(nvg::RGBA(220, 220, 220, 160));
	vg.Text(x + w / 2, y + 16, title.c_str(), NULL);

	vg.Restore();
}

void SearchBox::Render(nvg::Context& vg) const
{
	float cornerRadius = h / 2 - 1;

	// Edit
	nvg::paint bg = vg.BoxGradient(x, y + 1.5f, w, h, h / 2, 5, nvg::RGBA(0, 0, 0, 16), nvg::RGBA(0, 0, 0, 92));
	vg.BeginPath();
	vg.RoundedRect(x, y, w, h, cornerRadius);
	vg.FillPaint(bg);
	vg.Fill();

	vg.SetFontSize(h*1.3f);
	vg.SetFontFace("icons");
	vg.FillColor(nvg::RGBA(255, 255, 255, 64));
	vg.TextAlign(nvg::Align::CENTER | nvg::Align::MIDDLE);
	vg.Text(x + h*0.55f, y + h*0.55f, cpToUTF8(SEARCH).c_str(), NULL);

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 32));

	vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
	vg.Text(x + h*1.05f, y + h*0.5f, text.c_str(), NULL);

	vg.SetFontSize(h*1.3f);
	vg.SetFontFace("icons");
	vg.FillColor(nvg::RGBA(255, 255, 255, 32));
	vg.TextAlign(nvg::Align::CENTER | nvg::Align::MIDDLE);
	vg.Text(x + w - h*0.55f, y + h*0.55f, cpToUTF8(CIRCLED_CROSS).c_str(), NULL);
}

void DropDown::Render(nvg::Context& vg) const
{
	float cornerRadius = 4.0f;

	nvg::paint bg = vg.LinearGradient(x, y, x, y + h, nvg::RGBA(255, 255, 255, 16), nvg::RGBA(0, 0, 0, 16));
	vg.BeginPath();
	vg.RoundedRect(x + 1, y + 1, w - 2, h - 2, cornerRadius - 1);
	vg.FillPaint(bg);
	vg.Fill();

	vg.BeginPath();
	vg.RoundedRect(x + 0.5f, y + 0.5f, w - 1, h - 1, cornerRadius - 0.5f);
	vg.StrokeColor(nvg::RGBA(0, 0, 0, 48));
	vg.Stroke();

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 160));
	vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
	vg.Text(x + h*0.3f, y + h*0.5f, text.c_str(), NULL);

	vg.SetFontSize(h*1.3f);
	vg.SetFontFace("icons");
	vg.FillColor(nvg::RGBA(255, 255, 255, 64));
	vg.TextAlign(nvg::Align::CENTER | nvg::Align::MIDDLE);
	vg.Text(x + w - h*0.5f, y + h*0.5f, cpToUTF8(CHEVRON_RIGHT).c_str(), NULL);
}

void CheckBox::Render(nvg::Context& vg) const
{
	vg.SetFontSize(18.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 160));

	vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
	vg.Text(x + 28, y + h*0.5f, text.c_str(), NULL);

	nvg::paint bg = vg.BoxGradient(x + 1, y + (int)(h*0.5f) - 9 + 1, 18, 18, 3, 3, nvg::RGBA(0, 0, 0, 32), nvg::RGBA(0, 0, 0, 92));
	vg.BeginPath();
	vg.RoundedRect(x + 1, y + (int)(h*0.5f) - 9, 18, 18, 3);
	vg.FillPaint(bg);
	vg.Fill();

	vg.SetFontSize(40);
	vg.SetFontFace("icons");
	vg.FillColor(nvg::RGBA(255, 255, 255, 128));
	vg.TextAlign(nvg::Align::CENTER | nvg::Align::MIDDLE);
	vg.Text(x + 9 + 2, y + h*0.5f, cpToUTF8(CHECK).c_str(), NULL);
}

Button::Button()
    : state(State::RELEASED)
    , color(btnColorInactive)
    , userPtr(nullptr)
{
}

void Button::Render(nvg::Context& vg) const
{
	vg.BeginPath();

    float x = pos.x, y = pos.y;
    float w = extent.x, h = extent.y;
	
	const float cornerRadius = 4.0f;
	vg.RoundedRect(x + 1, y + 1, w - 2, h - 2, cornerRadius - 1);
	if (!IsBlack(color)) 
	{
		vg.FillColor(nvg::color(color.r, color.g, color.b, color.a));
		vg.Fill();
	}
	vg.FillPaint(vg.LinearGradient(x, y, x, y + h, nvg::RGBA(255, 255, 255, IsBlack(color) ? 16 : 32), nvg::RGBA(0, 0, 0, IsBlack(color) ? 16 : 32)));
	vg.Fill();

	vg.BeginPath();
	vg.RoundedRect(x + 0.5f, y + 0.5f, w - 1, h - 1, cornerRadius - 0.5f);
	vg.StrokeColor(nvg::RGBA(0, 0, 0, 48));
	vg.Stroke();

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans-bold");
	float tw = vg.TextBounds(0, 0, text.c_str(), nullptr)->advanceX;
	float iw = 0;
	if (preicon) 
	{
		vg.SetFontSize(h*1.3f);
		vg.SetFontFace("icons");
		iw = vg.TextBounds(0, 0, cpToUTF8(preicon).c_str(), nullptr)->advanceX;
		iw += h*0.15f;
	
		vg.SetFontSize(h*1.3f);
		vg.SetFontFace("icons");
		vg.FillColor(nvg::RGBA(255, 255, 255, 96));
		vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
		vg.Text(x + w*0.5f - tw*0.5f - iw*0.75f, y + h*0.5f, cpToUTF8(preicon).c_str(), NULL);
	}

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans-bold");
	vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
	vg.FillColor(nvg::RGBA(0, 0, 0, 160));
	vg.Text(x + w*0.5f - tw*0.5f + iw*0.25f, y + h*0.5f - 1, text.c_str(), NULL);
	vg.FillColor(nvg::RGBA(255, 255, 255, 160));
	vg.Text(x + w * 0.5f - tw * 0.5f + iw*0.25f, y + h*0.5f, text.c_str(), NULL);
}

bool Button::PointInBounds(const glm::vec2& point)
{
    return point.x > pos.x && point.y > pos.y && point.x < (pos + extent).x && point.y < (pos + extent).y;
}

Color btnColorInactive = Color(0, 96, 128, 255);
Color btnColorActive = Color(0, 128, 0, 255);
Color btnColorDisabled = Color(128, 128, 128, 255);

void Button::OnPress()
{
    if (state == State::DISABLED)
        return;

    state = State::PRESSED;
    color = btnColorActive;
}

void Button::OnRelease()
{
    if (state == State::DISABLED)
        return;

    state = State::RELEASED;
    color = btnColorInactive;

    callback(userPtr);
}

void Button::ToggleEnable()
{
    if (state == State::DISABLED)
    {
        state = State::RELEASED;
        color = btnColorInactive;
    }
    else
    {
        state = State::DISABLED;
        color = btnColorDisabled;
    }
}

void EditBoxBase::Render(nvg::Context& vg) const
{
	nvg::paint bg = vg.BoxGradient(x + 1, y + 1 + 1.5f, w - 2, h - 2, 3, 4, nvg::RGBA(255, 255, 255, 32), nvg::RGBA(32, 32, 32, 32));
	vg.BeginPath();
	vg.RoundedRect(x + 1, y + 1, w - 2, h - 2, 4 - 1);
	vg.FillPaint(bg);
	vg.Fill();

	vg.BeginPath();
	vg.RoundedRect(x + 0.5f, y + 0.5f, w - 1, h - 1, 4 - 0.5f);
	vg.StrokeColor(nvg::RGBA(0, 0, 0, 48));
	vg.Stroke();
}

void EditBox::Render(nvg::Context& vg) const
{
	EditBoxBase::Render(vg);

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 64));
	vg.TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
	vg.Text(x + h*0.3f, y + h*0.5f, text.c_str(), NULL);
}

void EditBoxNum::Render(nvg::Context& vg) const
{
	EditBoxBase::Render(vg);

	float uw = vg.TextBounds(0, 0, units.c_str(), nullptr)->advanceX;

	vg.SetFontSize(18.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 64));
	vg.TextAlign(nvg::Align::RIGHT | nvg::Align::MIDDLE);
	vg.Text(x + w - h*0.3f, y + h*0.5f, units.c_str(), NULL);

	vg.SetFontSize(20.0f);
	vg.SetFontFace("sans");
	vg.FillColor(nvg::RGBA(255, 255, 255, 128));
	vg.TextAlign(nvg::Align::RIGHT | nvg::Align::MIDDLE);
	vg.Text(x + w - uw - h*0.5f, y + h*0.5f, text.c_str(), NULL);
}

void Slider::Render(nvg::Context& vg) const
{
    const auto x = pos.x;
    const auto y = pos.y;
    const auto w = extent.x;
    const auto h = extent.y;

	vg.Save();

	// Slot
    const float cy = y + int(h * .5f);
	auto bg = vg.BoxGradient(x, cy - 2 + 1, w, 4, 2, 2, nvg::RGBA(0, 0, 0, 32), nvg::RGBA(0, 0, 0, 128));
	vg.BeginPath();
	vg.RoundedRect(x, cy - 2, w, 4, 2);
	vg.FillPaint(bg);
	vg.Fill();

	// Knob Shadow
    const float kr = int(h * .25f);
	bg = vg.RadialGradient(x + int(knobPos * w), cy + 1, kr - 3, kr + 3, nvg::RGBA(0, 0, 0, 64), nvg::RGBA(0, 0, 0, 0));
	vg.BeginPath();
	vg.CreateRect(x + int(knobPos * w) - kr - 5, cy - kr - 5, kr * 2 + 5 + 5, kr * 2 + 5 + 5 + 3);
	vg.Circle(x + int(knobPos * w), cy, kr);
	vg.PathWinding(nvg::NVG_HOLE);
	vg.FillPaint(bg);
	vg.Fill();

	// Knob
	const auto knob = vg.LinearGradient(x, cy - kr, x, cy + kr, nvg::RGBA(255, 255, 255, 16), nvg::RGBA(0, 0, 0, 16));
	vg.BeginPath();
	vg.Circle(x + int(knobPos * w), cy, kr - 1);
	vg.FillColor(nvg::RGBA(40, 43, 48, 255));
	vg.Fill();
	vg.FillPaint(knob);
	vg.Fill();

	vg.BeginPath();
	vg.Circle(x + int(knobPos * w), cy, kr - 0.5f);
	vg.StrokeColor(nvg::RGBA(0, 0, 0, 92));
	vg.Stroke();

	vg.Restore();
}

void Slider::OnDrag(const float newPos)
{
    callback(userPtr);
}

void Slider::TestDrag(const glm::vec2& mouse)
{
    const auto x = pos.x;
    const auto y = pos.y;
    const auto w = extent.x;
    const auto h = extent.y;
    const float cy = y + int(h * .5f);

    if (mouse.x > x && mouse.x < x + w &&
        mouse.y > y && mouse.y < y + h)
    {
        knobPos = (mouse.x - pos.x) / extent.x;
        knobPos = std::clamp(knobPos, 0.f, 1.f);
        dragging = true;
    }
}

bool IsPtInRect(glm::vec2 pt, glm::vec2 botLeft, glm::vec2 topRight)
{
	return pt.x > botLeft.x && 
		   pt.y > botLeft.y &&
		   pt.x < topRight.x &&
		   pt.y < topRight.y;
}

void Multiline::Render(nvg::Context& vg) const
{
	auto mx = UIManager::input->mouseX;
	auto my = UIManager::input->mouseY;
	auto y = yy;

	vg.Save();

	vg.SetFontSize(18.0f);
	vg.SetFontFace("sans");
	vg.TextAlign(nvg::Align::LEFT | nvg::Align::TOP);
	const auto textMetrics = vg.ComputeTextMetrics();
	const float lineh = textMetrics.lineh;

	// The text break API can be used to fill a large buffer of rows or to iterate over the text just few lines (or just one) at a time.
	// The "next" variable of the last returned item tells where to continue.
	float gx, gy;
	int gutter = 0;
	int lnum = 0;

	auto start = text.c_str();
	for(;;)
	{
		nvg::textRow rows[3];
		int nrows = vg.TextBreakLines(start, nullptr, width, rows, 3);
		if (!nrows) break;

		for(const auto& row : rows)
		{
			bool hit = IsPtInRect({ mx, my }, { x, y }, { x + width, y + lineh });

			vg.BeginPath();
			vg.FillColor(nvg::RGBA(255, 255, 255, hit ? 64 : 16));
			vg.CreateRect(x, y, row.width, lineh);
			vg.Fill();

			vg.FillColor(nvg::RGBA(255, 255, 255, 255));
			vg.Text(x, y, row.start, row.end);

			if (hit) 
			{
				float caretx = (mx < x + row.width / 2) ? x : x + row.width;
				float px = x;
				auto glyphs = *vg.TextGlyphPositions(x, y, row.start, row.end, 100);
				size_t nglyphs = glyphs.size();
				for (size_t i = 0; i < nglyphs; ++i) 
				{
					float x0 = glyphs[i].x;
					float x1 = (i + 1 < nglyphs) ? glyphs[i + 1].x : x + row.width;
					float gx = x0 * 0.3f + x1 * 0.7f;
					if (mx >= px && mx < gx)
						caretx = glyphs[i].x;
					px = gx;
				}

				vg.BeginPath();
				vg.FillColor(nvg::RGBA(255, 192, 0, 255));
				vg.CreateRect(caretx, y, 1, lineh);
				vg.Fill();

				gutter = lnum + 1;
				gx = x - 10;
				gy = y + lineh / 2;
			}
			lnum++;
			y += lineh;
		}
		// Keep going...
		start = rows[nrows - 1].next;
	}

	Rect bounds;
	if (gutter) 
	{
		char txt[16];
		snprintf(txt, sizeof(txt), "%d", gutter);
		vg.SetFontSize(13.0f);
		vg.TextAlign(nvg::Align::RIGHT | nvg::Align::MIDDLE);

		bounds = vg.TextBounds(gx, gy, txt, NULL)->bounds;

		vg.BeginPath();
		vg.FillColor(nvg::RGBA(255, 192, 0, 255));
		vg.RoundedRect((int)bounds.xmin - 4, (int)bounds.ymin - 2, (int)(bounds.xmax - bounds.xmin) + 8, (int)(bounds.ymax - bounds.ymin) + 4, ((int)(bounds.ymax - bounds.ymin) + 4) / 2 - 1);
		vg.Fill();

		vg.FillColor(nvg::RGBA(32, 32, 32, 255));
		vg.Text(gx, gy, txt, NULL);
	}

	y += 20.0f;

	vg.SetFontSize(13.0f);
	vg.TextAlign(nvg::Align::LEFT | nvg::Align::TOP);
	vg.TextLineHeight(1.2f);

	bounds = *vg.TextBoxBounds(x, y, 150, "Hover your mouse over the text to see calculated caret position.", NULL);
	
	// Fade the tooltip out when close to it.
	gx = fabsf((mx - (bounds.xmin + bounds.xmax)*0.5f) / (bounds.xmin - bounds.xmax));
	gy = fabsf((my - (bounds.ymin + bounds.ymax)*0.5f) / (bounds.ymin - bounds.ymax));
	float a = maxf(gx, gy) - 0.5f;
	a = clampf(a, 0, 1);
	vg.GlobalAlpha(a);

	vg.BeginPath();
	vg.FillColor(nvg::RGBA(220, 220, 220, 255));
	vg.RoundedRect(bounds.xmin - 2, bounds.ymin - 2, (int)(bounds.xmax - bounds.xmin) + 4, (int)(bounds.ymax - bounds.ymin) + 4, 3);
	float px = (int)((bounds.xmax + bounds.xmin) / 2);
	vg.MoveTo(px, bounds.ymin - 10);
	vg.LineTo(px + 7, bounds.ymin + 1);
	vg.LineTo(px - 7, bounds.ymin + 1);
	vg.Fill();

	vg.FillColor(nvg::RGBA(0, 0, 0, 220));
	vg.TextBox(x, y, 150, "Hover your mouse over the text to see calculated caret position.", NULL);

	vg.Restore();
}

void Multiline::UpdateScale(float fbWidth, float fbHeight)
{
	x = fbWidth - 350;
}

Graph::Graph(const std::string& name_, float upperLimit_, Color fillColor_, const std::string& units_, long long precision_, size_t maxVals, bool limitLower_)
{
    values.resize(maxVals);
    name = name_;
    upperLimit = upperLimit_;
    limitLower = limitLower_;
    fillColor = fillColor_;
    units = units_;
    precision = precision_;
    head = 0;
}

void Graph::Update(const float val)
{
    head = (head + 1) % values.size();
    values[head] = val;
}

void Graph::Render(nvg::Context& vg, const float x, const float y)
{
    const float w = 200 + 5 + 200;
    const float h = 70;

    DrawShape(vg, x, y, w, h);
    DrawText(vg, x, y, w);
}

void Graph::DrawShape(nvg::Context& vg, float x, float y, float w, float h)
{
    vg.BeginPath();
    vg.CreateRect(x, y, w, h);
    vg.FillColor(nvg::RGBA(0, 0, 0, 128));
    vg.Fill();

    vg.BeginPath();
    vg.MoveTo(x, y + h);

    const auto maxVals = values.size();
    for (size_t i = 0; i < maxVals; ++i)
    {
        auto v = values[(head + i) % maxVals];
        if (v > upperLimit) v = upperLimit;
        if (limitLower && v < 0.f) v = 0.f;
        vg.LineTo(x + float(i) / (maxVals - 1) * w,
                  y + h - v / upperLimit * h);
    }

    vg.LineTo(x + w, y + h);
    vg.FillColor(nvg::color(fillColor.rgba));
    vg.Fill();
}

void Graph::DrawText(nvg::Context& vg, float x, float y, float w)
{
    vg.SetFontFace("sans");

    assert(!name.empty());
    vg.SetFontSize(14.f);
    vg.TextAlign(nvg::Align::LEFT | nvg::Align::TOP);
    vg.FillColor(nvg::RGBA(240, 240, 240, 192));
    vg.Text(x + 3, y + 1, name.c_str(), nullptr);

    vg.SetFontSize(18.0f);
    vg.TextAlign(nvg::Align::RIGHT | nvg::Align::TOP);
    vg.FillColor(nvg::RGBA(240, 240, 240, 255));

    std::stringstream val;
    val << std::fixed << std::setprecision(precision) << values[head] << " " << units;
    vg.Text(x + w - 3, y + 1, val.str().c_str(), nullptr);
}

Color VIS_API ColorRGBA(float r, float g, float b, float a)
{
	return Color(r / 255.f, g / 255.f, b / 255.f, a / 255.f);
}
