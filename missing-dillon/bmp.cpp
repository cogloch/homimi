#include "bmp.hpp"
#include "common.hpp"
#include "Arduino.h"
#include <Wire.h>
#include <stdio.h>
#include <math.h>


const u8 BMP180_ADDR = 0x77;

enum BmpRegister : u8
{
    Control = 0xF4,
    Result = 0xF6
};

enum Command : u8
{
    Temperature = 0x2E,
    Pressure = 0x34
};

void ReadBytes(u8* val, u8 length)
{
    Wire.beginTransmission(BMP180_ADDR);
    Wire.write(val[0]);
    Wire.endTransmission();
    Wire.requestFrom(BMP180_ADDR, length);
    while (Wire.available() != length);
    for (u8 x = 0; x < length; x++)
        val[x] = Wire.read();
}

void WriteBytes(u8* val, u8 length)
{
    Wire.beginTransmission(BMP180_ADDR);
    Wire.write(val, length);
    Wire.endTransmission();
}

// Calibration data for THIS board only. 
const double c5 = 0.0047281270;
const double c6 = 21505.0000000000;

const double mc = -942.8800048828;
const double md = 18.4249992370;

const double x0 = 8724.0000000000;
const double x1 = -21.6406250000;
const double x2 = 0.0312805175;

const double y0 = 33.9830017089;
const double y1 = -0.0742362117;
const double y2 = 0.0001610892;

const double p0 = 2.3643751144;
const double p1 = 0.9929838180;
const double p2 = 0.0000044209;

BmpSensor::BmpSensor()
{
    Wire.begin();
}

void BmpSensor::Tick()
{
    // Start temperature 
    u8 data[] = { BmpRegister::Control, Command::Temperature };
    WriteBytes(data, 2);
    delay(5);

    // Get temperature 
    data[0] = BmpRegister::Result; data[1] = 0;
    ReadBytes(data, 2);
    // From Bosch datasheet : tu = 27898;
    // From http://wmrx00.sourceforge.net/Arduino/BMP085-Calcs.pdf : tu = 0x69EC;
    const double tu = (data[0] * 256.0) + data[1];
    const double a = c5 * (tu - c6);
    temp = a + (mc / (a + md));

    // Start pressure 
    data[0] = BmpRegister::Control; data[1] = Command::Pressure;
    WriteBytes(data, 2);
    delay(5);
    
    // Get pressure
    u8 data2[3];
    data2[0] = BmpRegister::Result;
    ReadBytes(data2, 3);

    // From Bosch datasheet : pu = 23843;
    // From http://wmrx00.sourceforge.net/Arduino/BMP085-Calcs.pdf : pu = 0x982FC0; pu = (0x98 * 256.0) + 0x2F + (0xC0/256.0);
    const float pu = (data2[0] * 256.0) + data2[1] + (data2[2] / 256.0);

    const double s = temp - 25.0;
    const double sSq = s * s;
    const double x = (x2 * sSq) + (x1 * s) + x0;
    const double y = (y2 * sSq) + (y1 * s) + y0;
    const double z = (pu - x) / y;
    pressure = (p2 * pow(z, 2)) + (p1 * z) + p0;
}
