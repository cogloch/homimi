#pragma once


class BmpSensor
{
public:
    BmpSensor();
    void Tick();
    float pressure = 0.0;
    float temp = 0.0;
};