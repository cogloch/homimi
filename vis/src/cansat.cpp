#include "stdafx.h"
#include "cansat.h"
#include "shader.h"
#include "timing.h"

#include <glm/gtc/matrix_transform.hpp>

#include "xlsxwriter.h"
#pragma comment(lib, "LibXlsxWriter.lib")


const glm::vec3 CanState::invalidVec = glm::vec3(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
glm::vec3 CanState::baseAcc = invalidVec;
int CanState::readings = 0;

Shader* shader;
GLint projLoc, viewLoc, modelLoc;

void Cansat::Init()
{
    shader = new Shader("phong");

    projLoc = shader->GetUniformLoc("projection");
    viewLoc = shader->GetUniformLoc("view");
    modelLoc = shader->GetUniformLoc("model");
}

#include <glm/gtx/quaternion.hpp>

void Cansat::Draw(glm::vec2 winExtent, CanState& state, Model& mesh)
{
    gl::Enable(gl::DEPTH_TEST);
    shader->Use();

    glm::mat4 view = translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -10.0f));
    shader->SetMat4(viewLoc, view);
    
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), winExtent.x / winExtent.y, 0.1f, 100.0f);
    shader->SetMat4(projLoc, projection);

    glm::mat4 model;
    model = glm::toMat4(state.quat);
    /*model = rotate(model, pitch(state.quat), { 0.f, 1.f, 0.f });
    model = rotate(model, yaw(state.quat), { 1.f, 0.f, 0.f });
    model = rotate(model, roll(state.quat), { 0.f, 0.f, 1.f });*/
    model = translate(model, state.pos);
    shader->SetMat4(modelLoc, model);
    
    mesh.Draw(shader->program);
}

void TimelineMgr::OnRecordKey()
{
    if (state == State::RECORDING)
    {
        // Stop current timeline from being recorded 
        state = State::INACTIVE;
    }
    else
    {
        // Start a new timeline recording 
        state = State::RECORDING;
        selected = AddTimeline();
    }

    currentFrame = 0;
    playbackStart = std::chrono::steady_clock::now();
}

void TimelineMgr::OnPlayKey()
{
    if (state == State::PLAYING)
        state = State::INACTIVE;
    else
        state = State::PLAYING;

    playbackStart = std::chrono::steady_clock::now();
}

#include <shobjidl.h> 

void TimelineMgr::OnLoad()
{
    IFileOpenDialog* fileOpenDlg = nullptr;
    IShellItem* item = nullptr;
    IShellItem* workDir = nullptr;
    wchar_t* filepath = nullptr;

    try
    {
        auto hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
        if (FAILED(hr)) throw "COM library init failed.";

        hr = CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&fileOpenDlg));
        if (FAILED(hr)) throw "File open dialog init failed.";

        COMDLG_FILTERSPEC fileType = { L"homimi file", L"*.cuc" };
        hr = fileOpenDlg->SetFileTypes(1, &fileType);
        
        wchar_t workDirName[50];
        GetCurrentDirectory(50, workDirName);
        hr = SHCreateItemFromParsingName(workDirName, nullptr, IID_PPV_ARGS(&workDir));
        if (FAILED(hr)) throw "Failed creating shell item for work dir.";
        
        hr = fileOpenDlg->SetFolder(workDir);
        if (FAILED(hr)) throw "Failed setting the open file dialog default folder to the work dir.";

        hr = fileOpenDlg->Show(nullptr);
        if (FAILED(hr)) throw "File open dialog display failed.";

        hr = fileOpenDlg->GetResult(&item);
        if (FAILED(hr)) throw "File open dialog picking failed.";

        hr = item->GetDisplayName(SIGDN_FILESYSPATH, &filepath);
        if (FAILED(hr)) throw "Failed getting the display name of the picked item.";

        LoadTimeline(filepath);
    }
    catch (...)
    {
    }

    if (filepath) CoTaskMemFree(filepath);
    if (item) item->Release();
    if (workDir) workDir->Release();
    if (fileOpenDlg) fileOpenDlg->Release();
    CoUninitialize();
}

void TimelineMgr::JumpTo(float point)
{
    std::cout << "Jump to " << point * 100 << "%\n";
    if (state == State::PLAYING)
        state = State::PAUSED;
}

size_t TimelineMgr::AddTimeline()
{
    timelines.push_back(Timeline());
    return timelines.size() - 1;
}

void TimelineMgr::AddState(CanState state)
{
    timelines[selected].states.push_back(state);
}

void TimelineMgr::RunIntegrator()
{
    // 1 unit = 1 can height = 10cm
    // 1 unit = 0.1m
    const float mToUnit = 0.1f;

    auto& states = timelines[selected].states;

    if (states.size() > 1)
    {
        // Find first untouched state
        size_t startInterIdx = 1;
        for (; startInterIdx < states.size(); ++startInterIdx)
        {
            if (states[startInterIdx].vel == glm::vec3(0.f, 0.f, 0.f))
                break;
        }
        
        for (size_t i = startInterIdx; i < states.size(); ++i)
        {
            auto& state = states[i];
            auto& prev = states[i - 1];
            float dt = float(state.timestamp - prev.timestamp) / 1000.f; // ms->s
            if (dt > 2.f) dt = 0.01f; // Most likely initial frame 
            state.vel = prev.vel + (state.acc * 9.81f) * dt * mToUnit; // g->m/s^2
            state.pos = prev.pos + state.vel * dt * mToUnit;
        }
    }
}

size_t TimelineMgr::LoadTimeline(const std::wstring& name)
{
    std::ifstream file(name, std::ios::binary);

    Timeline timeline;
    char buffer[sizeof(CanState)];
    CanState state;
    while (file.read(buffer, sizeof(CanState)))
    {
        memcpy(&state, buffer, sizeof(CanState));
        timeline.states.push_back(state);
    }

    // TODO Only one timeline at any time for now 
    if (timelines.empty()) timelines.push_back(timeline);
    else timelines[selected] = timeline;

    return timelines.size() - 1;
}

void TimelineMgr::DumpTimeline(size_t id)
{
    auto& states = timelines[id].states;

    // homimi format 
    const auto datetime = time(nullptr);
    std::string filename(ctime(&datetime));
    filename.erase(filename.begin() + filename.size() - 1);
    for (auto& ch : filename)
        if (ch == ':')
            ch = '_';
    std::ofstream file(filename + ".cuc", std::ios::out | std::ios::binary);
    for (auto& state : states)
        file.write(reinterpret_cast<char*>(&state), sizeof(CanState));

    // Excel 
    const auto workbook = workbook_new((filename + ".xlsx").c_str());
    const auto worksheet = workbook_add_worksheet(workbook, nullptr);

    worksheet_write_string(worksheet, 0, 0, "Timestamp", nullptr);
    worksheet_write_string(worksheet, 0, 1, "Pressure", nullptr);
    worksheet_write_string(worksheet, 0, 2, "Temperature", nullptr);
    worksheet_write_string(worksheet, 0, 3, "Yaw", nullptr);
    worksheet_write_string(worksheet, 0, 4, "Pitch", nullptr);
    worksheet_write_string(worksheet, 0, 5, "Roll", nullptr);
    worksheet_write_string(worksheet, 0, 6, "Accel X", nullptr);
    worksheet_write_string(worksheet, 0, 7, "Accel Y", nullptr);
    worksheet_write_string(worksheet, 0, 8, "Accel Z", nullptr);

    for (int i = 0; i < states.size(); ++i)
    {
        worksheet_write_number(worksheet, i + 1, 0, states[i].timestamp, nullptr);
        worksheet_write_number(worksheet, i + 1, 1, states[i].pressure, nullptr);
        worksheet_write_number(worksheet, i + 1, 2, states[i].temp, nullptr);
        worksheet_write_number(worksheet, i + 1, 3, glm::degrees(yaw(states[i].quat)), nullptr);
        worksheet_write_number(worksheet, i + 1, 4, glm::degrees(pitch(states[i].quat)), nullptr);
        worksheet_write_number(worksheet, i + 1, 5, glm::degrees(roll(states[i].quat)), nullptr);
        worksheet_write_number(worksheet, i + 1, 6, states[i].acc.x, nullptr);
        worksheet_write_number(worksheet, i + 1, 7, states[i].acc.y, nullptr);
        worksheet_write_number(worksheet, i + 1, 8, states[i].acc.z, nullptr);
    }

    workbook_close(workbook);
}

void TimelineMgr::SelectTimeline(size_t id)
{
    selected = id;
}

void TimelineMgr::DeleteTimeline(size_t id)
{
    timelines.erase(timelines.begin() + id);
    if (selected == id)
        selected = 0;
}

void TimelineMgr::OnExport()
{
    DumpTimeline(selected);
}
