#pragma once
#include "flight.h"
#include <optional>
#include <array>
#include <cmath>


namespace atmo
{
	struct Layer
	{
		// Geometric altitude at the base of the layer
		// For h < 65km, geometric and geopotential altitude virtually the same; above, h_g = h * (r_0 / (r_0 + h))
		// In km
		double h;

		// Temperature 
		// In K 
		double T;

		// Pressure
		// In Pa
		double p;

		// Density 
		// In kg/m^3
		double rho;

		// Thermal lapse rate
		// In K/km
		double a;

		// Specific gas constant 
		// In J/(kgK)
		double R;
	};

	// Earth standard atmosphere: averaged horizontal & temporal variations; vertical variations for rho, T, p
	// Reasonably accurate up to 86km, with a linear variation of temperature vs altitude, based on the US Standard Atmosphere 1976: https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19770009539.pdf
	// Between 86km..2000km, nonlinear variation of temperature vs altitude, assume linear based on the US Standard Atmosphere 1962: https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19630003300.pdf
	constexpr std::array<Layer, 22> tempLayers = { {
		// h[km]   T[K]    p[Pa] rho[kg/m^3] a[K/km] R[J/(kgK)]
		{ 0.0,     288.15,  0.0, 0.0,        -6.5,  287.0 },
		{ 11.0191, 216.65,  0.0, 0.0,		 0.0,   287.0 },
		{ 20.0631, 216.65,  0.0, 0.0,		 1.0,   287.0 },
		{ 32.1619, 228.65,  0.0, 0.0,		 2.8,   287.0 },
		{ 47.3501, 270.65,  0.0, 0.0,		 0.0,   287.0 },
		{ 51.4125, 270.65,  0.0, 0.0,		 -2.8,  287.0 },
		{ 71.8020, 214.65,  0.0, 0.0,		 -2.0,  287.02 },
		{ 86.0,    186.946, 0.0, 0.0,		 1.693, 287.02 },
		// Spooky stuff above this altitude
		{ 100.0,   210.02, 0.0, 0.0,		 5.0,   287.84 },
		{ 110.0,   257.0,  0.0, 0.0,		 10.0,  291.06 },
		{ 120.0,   349.49, 0.0, 0.0,		 20.0,  308.79 },
		{ 150.0,   892.79, 0.0, 0.0,	 	 15.0,  311.80 },
		{ 160.0,   1022.2, 0.0, 0.0,		 10.0,  313.69 },
		{ 170.0,   1103.4, 0.0, 0.0,	 	 7.0,   321.57 },
		{ 190.0,   1205.4, 0.0, 0.0,		 5.0,   336.68 },
		{ 230.0,   1322.3, 0.0, 0.0,		 4.0,   366.84 },
		{ 300.0,   1432.1, 0.0, 0.0,		 3.3,   416.88 },
		{ 400.0,   1487.4, 0.0, 0.0,		 2.6,   463.36 },
		{ 500.0,   1506.1, 0.0, 0.0,		 1.7,   493.63 },
		{ 600.0,   1506.1, 0.0, 0.0,		 1.1,   514.08 },
		{ 700.0,   1507.6, 0.0, 0.0,		 0.0,   514.08 },
		// Here be dragons 
		{ 2000.0,  0.0,    0.0, 0.0,		 0.0,   0.0 }
		} };

	// Gravitational acceleration at sea-level
	// In m/s^2
	const double g0 = 9.80665;

	// Nominal equatorial Earth radius: https://arxiv.org/pdf/1510.07674.pdf
	// In m 
	const double ree = 6378100.0;

	constexpr std::array<Layer, tempLayers.size()> PopulateLayers()
	{
		std::array<Layer, tempLayers.size()> layers = { 0.0 };
		 
		layers[0].T = tempLayers[0].T;
		layers[0].R = tempLayers[0].R;
		layers[0].a = tempLayers[0].a;
		layers[0].p = 101325.0;
		layers[0].rho = layers[0].p / (layers[0].R * layers[0].T);
		layers[0].h = tempLayers[0].h;

		for (size_t i = 0, numLayers = tempLayers.size() - 1; i < numLayers; ++i)
		{
			layers[i + 1].a = tempLayers[i + 1].a;
			layers[i + 1].h = tempLayers[i + 1].h;
			layers[i + 1].R = tempLayers[i + 1].R;
			layers[i + 1].T = tempLayers[i + 1].T;

			const double dh = layers[i + 1].h - layers[i].h;
			const double R = layers[i + 1].R;
			const double a = layers[i + 1].a;
			const double T = layers[i + 1].T;
			const double Ti = layers[i].T;

			if (layers[i + 1].a == 0)
			{
				//tex: Isothermal layer 
				// $$p = p_i \exp\bigg(-g_0 \frac{\Delta h}{R T_i}\frac{r - \Delta h}{r}\bigg)= C p_{i}$$
				// $$ \rho = \frac{p}{RT} = C\frac{p_i}{RT} = C \rho_{i}$$
				// $$ p = p_{i+1} \text{, } \rho = \rho_{i+1}$$
				// $$ R = R_{i+1}$$
				// $$r = r_{eE} = \text{Nominal equatorial Earth radius}$$ 
				// $$\Delta h = h_{i + 1} - h_i$$
				const double C = exp(-g0 *
					dh / (R * Ti) *
					(ree - dh) / ree
				);

				layers[i + 1].p = layers[i].p * C;
				layers[i + 1].rho = layers[i].rho * C;
			}
			else
			{
				//tex: Nonisothermal layer
				// $$ p = p_{i} \exp\bigg({\frac{2 g_0 \Delta h}{a R r }}\bigg) \bigg(1 + \frac{a \Delta h}{R T_i}\bigg)^{-g_0 \frac{r a + 2 (T_i - h_i a)}{a^2 R r}} = p_{i} E \bigg(1 + \frac{a \Delta h}{R T_i}\bigg)^C$$
				// $$ \rho = \rho_i E \bigg(\frac{T}{T_i}\bigg)^{C-1}$$
				// $$ p = p_{i+1} \text{, } \rho = \rho_{i+1}$$
				// $$ a = a_{i+1} \text{, } R = R_{i+1} \text{, } T = T_{i+1}$$
				// $$ r = r_{eE} = \text{Nominal equatorial Earth radius}$$
				// $$\Delta h = h_{i + 1} - h_i$$
				const double E = exp(2 * g0 * dh / (a * R * ree));
				const double C = -g0 * (ree * a + 2 * (Ti - layers[i].h * a)) / (a * a * R * ree);

				layers[i + 1].p = layers[i].p * E * pow(1 + a * dh / (R * Ti), C);
				layers[i + 1].rho = layers[i].rho * E * pow(T / Ti, C - 1);
			}
		}

		return layers;
	}

	const std::array<Layer, tempLayers.size()> layers = PopulateLayers();

	struct State
	{
		// Temperature
		// In K
		double T;

		// Pressure
		// In Pa
		double p;

		// Density 
		// In kg/m^3
		double rho;
	};

	// Altitude in km 
	std::optional<State> FLY_API GetStateAtAltitude(double alt);
};
