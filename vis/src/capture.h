#pragma once
#include <string>


void SaveScreenShot(int width, int height, const std::string& name);

struct ImageBuffer
{
	u8* data;
	int width, height;

	ImageBuffer(const int width, const int height);
	~ImageBuffer();
	void FlipHorizontal();
	void UnpremultiplyAlpha();
	void SetAlpha(const u8 alpha);
};