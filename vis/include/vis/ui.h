#pragma once
#undef DrawText

#include <glm\glm.hpp>
#include <string>
#include <vector>
#include <memory>
#include "vis.h"
#include <functional>


enum Icons
{
	SEARCH = 0x1F50D,
	CIRCLED_CROSS = 0x2716,
	CHEVRON_RIGHT = 0xE75E,
	CHECK = 0x2713,
	LOGIN = 0xE740,
	TRASH = 0xE729
};

namespace nvg
{
	struct Context;
}

struct VIS_API UIElement
{
	/*enum class Type
	{
		LABEL,
		WINDOW
	} type;
	size_t idx;*/

	virtual ~UIElement() = default;
	virtual void Render(nvg::Context&) const = 0;
	virtual void UpdateScale(float fbWidth, float fbHeight) {}
};

struct VIS_API Color
{
	Color() = default;
	Color(u8 r, u8 g, u8 b, u8 a); // 0..255

	union
	{
		float rgba[4];
		struct { float r, g, b, a; };
	};
};

// in 0..255
Color VIS_API ColorRGBA(float r, float g, float b, float a);

struct VIS_API Window : UIElement
{
	std::string title;
	float x, y, w, h;

	void Render(nvg::Context&) const override;
};

struct VIS_API Label : UIElement
{
	float fontSize;
	std::string fontFace;
	Color color;
	int align;

	std::string text;
	float x, y;
	float width, height;

	void Render(nvg::Context&) const override;
};

struct VIS_API SearchBox : UIElement
{
	std::string text;
	float x, y, w, h;

	void Render(nvg::Context&) const override;
};

struct VIS_API DropDown : UIElement
{
	std::string text;
	float x, y, w, h;

	void Render(nvg::Context&) const override;
};

struct VIS_API CheckBox : UIElement
{
	std::string text;
	float x, y, w, h;

	void Render(nvg::Context&) const override;
};

extern Color btnColorInactive;
extern Color btnColorActive;
extern Color btnColorDisabled;

struct VIS_API Button : UIElement
{
	int preicon = 0;
	std::string text;
    glm::vec2 pos, extent;
    Color color;

    Button();
	void Render(nvg::Context&) const override;

    bool PointInBounds(const glm::vec2& point);

    enum class State
    {
        DISABLED,
        RELEASED,
        PRESSED
    } state;

    void OnPress();
    void OnRelease();
    void ToggleEnable();

    std::function<void(void*)> callback;
    void* userPtr;
};

// A collection of widgets, could be a HUD view, menu screen, etc. 
// TODO caching, batching 
// For now elements are shoved into a stack(of queues) to mirror nanovg(TODO x2), will turn it into a graph later 
class VIS_API Screen
{
public:
	//void Render(nvg::Context&);

	std::vector<std::shared_ptr<UIElement>> elements;
	int activeElementId = -1;

	//void AddLabel(Label);
	//void AddWindow(Window);

	// Rendered front to back 
	//std::vector<UIElement> elements;
	//std::vector<Label> labels;
	//std::vector<Window> windows;
};

struct InputState;

struct VIS_API UIManager
{
	/*temp*/ static InputState* input;
};

struct VIS_API EditBoxBase : UIElement
{
	float x, y;
	float w, h;

	virtual void Render(nvg::Context&) const override;
};

struct VIS_API EditBox : EditBoxBase
{
	std::string text;

	void Render(nvg::Context&) const override;
};

struct VIS_API EditBoxNum : EditBoxBase
{
	std::string text;
	std::string units;

	void Render(nvg::Context&) const override;
};

struct VIS_API Slider : UIElement
{
	float knobPos = 0.f;
    glm::vec2 pos, extent;
    bool dragging = false;
    std::function<void(void*)> callback;
    void* userPtr;

	void Render(nvg::Context&) const override;
    void OnDrag(const float newPos);
    void TestDrag(const glm::vec2& mouse);
};

// TODO merge with Label, make it wrappable 
struct VIS_API Multiline : UIElement
{
	float x, yy;
	float width, height;
	std::string text;

	void Render(nvg::Context&) const  override;
	void UpdateScale(float fbWidth, float fbHeight) override;
};

class VIS_API Graph
{
public:
    explicit Graph(const std::string& name, float upperLimit, Color fillColor, const std::string& units, long long precision, size_t maxVals = 50, bool limitLower = true);
    void Update(const float val);
    void Render(nvg::Context&, const float x, const float y);

private:
    std::string name;
    std::vector<float> values;
    size_t head;
    float upperLimit;
    bool limitLower;
    Color fillColor;
    std::string units;
    long long precision;

    void DrawShape(nvg::Context& vg, float x, float y, float w, float h);
    void DrawText(nvg::Context& vg, float x, float y, float w);
};