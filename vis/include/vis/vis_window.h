#pragma once
#include "vis.h"

struct GLFWwindow;

class VIS_API VisWindow
{
public:
	VisWindow();
	~VisWindow();

	void MakeCurrent() const;
	void Loop();

	void CopyResources();

private:
	static bool s_glfwInit;
	GLFWwindow* m_window; // unique ptr 
};
