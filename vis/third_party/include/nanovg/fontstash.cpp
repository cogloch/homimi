#include "stdafx.h"
#include "fontstash.h"



#ifdef FONS_USE_FREETYPE

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_ADVANCES_H
#include <math.h>

struct FONSttFontImpl {
	FT_Face font;
};
typedef struct FONSttFontImpl FONSttFontImpl;

FT_Library ftLibrary;

int fons__tt_init(fons::Context *context)
{
	FT_Error ftError;
	FONS_NOTUSED(context);
	ftError = FT_Init_FreeType(&ftLibrary);
	return ftError == 0;
}

int fons__tt_loadFont(fons::Context *context, FONSttFontImpl *font, u8 *data, int dataSize)
{
	FT_Error ftError;
	FONS_NOTUSED(context);

	//font->font.userdata = stash;
	ftError = FT_New_Memory_Face(ftLibrary, (const FT_Byte*)data, dataSize, 0, &font->font);
	return ftError == 0;
}

void fons__tt_getFontVMetrics(FONSttFontImpl *font, int *ascent, int *descent, int *lineGap)
{
	*ascent = font->font->ascender;
	*descent = font->font->descender;
	*lineGap = font->font->height - (*ascent - *descent);
}

float fons__tt_getPixelHeightScale(FONSttFontImpl *font, float size)
{
	return size / (font->font->ascender - font->font->descender);
}

int fons__tt_getGlyphIndex(FONSttFontImpl *font, int codepoint)
{
	return FT_Get_Char_Index(font->font, codepoint);
}

int fons__tt_buildGlyphBitmap(FONSttFontImpl *font, int glyph, float size, float scale,
	int *advance, int *lsb, int *x0, int *y0, int *x1, int *y1)
{
	FT_Error ftError;
	FT_GlyphSlot ftGlyph;
	FT_Fixed advFixed;
	FONS_NOTUSED(scale);

	ftError = FT_Set_Pixel_Sizes(font->font, 0, (FT_UInt)(size * (float)font->font->units_per_EM / (float)(font->font->ascender - font->font->descender)));
	if (ftError) return 0;
	ftError = FT_Load_Glyph(font->font, glyph, FT_LOAD_RENDER);
	if (ftError) return 0;
	ftError = FT_Get_Advance(font->font, glyph, FT_LOAD_NO_SCALE, &advFixed);
	if (ftError) return 0;
	ftGlyph = font->font->glyph;
	*advance = (int)advFixed;
	*lsb = (int)ftGlyph->metrics.horiBearingX;
	*x0 = ftGlyph->bitmap_left;
	*x1 = *x0 + ftGlyph->bitmap.width;
	*y0 = -ftGlyph->bitmap_top;
	*y1 = *y0 + ftGlyph->bitmap.rows;
	return 1;
}

void fons__tt_renderGlyphBitmap(FONSttFontImpl *font, u8 *output, int outWidth, int outHeight, int outStride,
	float scaleX, float scaleY, int glyph)
{
	FT_GlyphSlot ftGlyph = font->font->glyph;
	int ftGlyphOffset = 0;
	int x, y;
	FONS_NOTUSED(outWidth);
	FONS_NOTUSED(outHeight);
	FONS_NOTUSED(scaleX);
	FONS_NOTUSED(scaleY);
	FONS_NOTUSED(glyph);	// glyph has already been loaded by fons__tt_buildGlyphBitmap

	for (y = 0; y < ftGlyph->bitmap.rows; y++) {
		for (x = 0; x < ftGlyph->bitmap.width; x++) {
			output[(y * outStride) + x] = ftGlyph->bitmap.buffer[ftGlyphOffset++];
		}
	}
}

int fons__tt_getGlyphKernAdvance(FONSttFontImpl *font, int glyph1, int glyph2)
{
	FT_Vector ftKerning;
	FT_Get_Kerning(font->font, glyph1, glyph2, FT_KERNING_DEFAULT, &ftKerning);
	return (int)((ftKerning.x + 32) >> 6);  // Round up and convert to integer
}

#else

#define STB_TRUETYPE_IMPLEMENTATION
void* fons__tmpalloc(size_t size, void* up);
void fons__tmpfree(void* ptr, void* up);
#define STBTT_malloc(x,u)    fons__tmpalloc(x,u)
#define STBTT_free(x,u)      fons__tmpfree(x,u)
#include "stb_truetype.h"

#ifdef STB_TRUETYPE_IMPLEMENTATION

void* fons__tmpalloc(size_t size, void* up);
void fons__tmpfree(void* ptr, void* up);

#endif // STB_TRUETYPE_IMPLEMENTATION

struct FONSttFontImpl 
{
	stbtt_fontinfo font;
};

int fons__tt_init(fons::Context* context)
{
	FONS_NOTUSED(context);
	return 1;
}

int fons__tt_loadFont(fons::Context* context, FONSttFontImpl *font, u8 *data, int dataSize)
{
	int stbError;
	FONS_NOTUSED(dataSize);

	font->font.userdata = context;
	stbError = stbtt_InitFont(&font->font, data, 0);
	return stbError;
}

void fons__tt_getFontVMetrics(FONSttFontImpl *font, int *ascent, int *descent, int *lineGap)
{
	stbtt_GetFontVMetrics(&font->font, ascent, descent, lineGap);
}

float fons__tt_getPixelHeightScale(FONSttFontImpl *font, float size)
{
	return stbtt_ScaleForPixelHeight(&font->font, size);
}

int fons__tt_getGlyphIndex(FONSttFontImpl *font, int codepoint)
{
	return stbtt_FindGlyphIndex(&font->font, codepoint);
}

int fons__tt_buildGlyphBitmap(FONSttFontImpl *font, int glyph, float size, float scale,
	int *advance, int *lsb, int *x0, int *y0, int *x1, int *y1)
{
	FONS_NOTUSED(size);
	stbtt_GetGlyphHMetrics(&font->font, glyph, advance, lsb);
	stbtt_GetGlyphBitmapBox(&font->font, glyph, scale, scale, x0, y0, x1, y1);
	return 1;
}

void fons__tt_renderGlyphBitmap(FONSttFontImpl *font, u8 *output, int outWidth, int outHeight, int outStride,
	float scaleX, float scaleY, int glyph)
{
	stbtt_MakeGlyphBitmap(&font->font, output, outWidth, outHeight, outStride, scaleX, scaleY, glyph);
}

int fons__tt_getGlyphKernAdvance(FONSttFontImpl *font, int glyph1, int glyph2)
{
	return stbtt_GetGlyphKernAdvance(&font->font, glyph1, glyph2);
}

#endif

unsigned int fons::hashint(unsigned int a)
{
	a += ~(a << 15);
	a ^= (a >> 10);
	a += (a << 3);
	a ^= (a >> 6);
	a += ~(a << 11);
	a ^= (a >> 16);
	return a;
}

int fons::mini(int a, int b)
{
	return a < b ? a : b;
}

int fons::maxi(int a, int b)
{
	return a > b ? a : b;
}

namespace fons
{
	struct Font
	{
		Font();
		~Font();

		FONSttFontImpl font;
		std::string name;
		u8* data;
		int dataSize;
		u8 freeData;
		float ascender;
		float descender;
		float lineh;
		Glyph* glyphs;
		int cglyphs;
		int nglyphs;
		std::array<int, HASH_LUT_SIZE> lut;
		std::array<int, MAX_FALLBACKS> fallbacks;
		int nfallbacks;
	};

	Params::Params(const int width, const int height, const u8 flags)
	{
		memset(this, 0, sizeof(*this));
		this->width = width;
		this->height = height;
		this->flags = flags;
	}
}

int fons::Context::GetFontByName(const std::string& name) const
{
	for (int i = 0, numFonts = fonts.size(); i < numFonts; ++i)
		if (name == fonts[i]->name)
			return i;

	return FONS_INVALID;
}

#ifdef STB_TRUETYPE_IMPLEMENTATION

void* fons__tmpalloc(size_t size, void* up)
{
	// 16-byte align the returned pointer
	size = (size + 0xf) & ~0xf;

	fons::Context* stash = (fons::Context*)up;
	if (stash->nscratch + (int)size > fons::SCRATCH_BUF_SIZE) 
	{
		if (stash->handleError)
			stash->handleError(stash->errorUptr, fons::SCRATCH_FULL, stash->nscratch + (int)size);
		return NULL;
	}
	u8* ptr = stash->scratch + stash->nscratch;
	stash->nscratch += (int)size;
	return ptr;
}

void fons__tmpfree(void* ptr, void* up)
{
	(void)ptr;
	(void)up;
	// empty
}

#endif // STB_TRUETYPE_IMPLEMENTATION

// Copyright (c) 2008-2010 Bjoern Hoehrmann <bjoern@hoehrmann.de>
// See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.
const int UTF8_ACCEPT = 0;
const int UTF8_REJECT = 12;

unsigned int fons__decutf8(unsigned int* state, unsigned int* codep, unsigned int byte)
{
	const u8 utf8d[] = {
		// The first part of the table maps bytes to character classes that
		// to reduce the size of the transition table and create bitmasks.
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
		7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
		8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
		10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3, 11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8,

		// The second part is a transition table that maps a combination
		// of a state of the automaton and a character class to a state.
		0,12,24,36,60,96,84,12,12,12,48,72, 12,12,12,12,12,12,12,12,12,12,12,12,
		12, 0,12,12,12,12,12, 0,12, 0,12,12, 12,24,12,12,12,12,12,24,12,24,12,12,
		12,12,12,12,12,12,12,24,12,12,12,12, 12,24,12,12,12,12,12,12,12,24,12,12,
		12,12,12,12,12,12,12,36,12,36,12,12, 12,36,12,12,12,12,12,36,12,36,12,12,
		12,36,12,12,12,12,12,12,12,12,12,12,
	};

	unsigned int type = utf8d[byte];

	*codep = (*state != UTF8_ACCEPT) ?
		(byte & 0x3fu) | (*codep << 6) :
		(0xff >> type) & (byte);

	*state = utf8d[256 + *state + type];
	return *state;
}

// Atlas based on Skyline Bin Packer by Jukka Jyl�nki

void fons__deleteAtlas(fons::Atlas* atlas)
{
	if (!atlas) return;
	if (atlas->nodes) delete[] atlas->nodes;
	delete atlas;
}


fons::Atlas::Node::Node()
{
	memset(this, 0, sizeof(*this));
}

fons::Atlas::Atlas(const int w, const int h, const int nnodes)
{
	memset(this, 0, sizeof(*this));

	width = w;
	height = h;

	// Skyline nodes
	nodes = new Node[nnodes];
	cnodes = nnodes;

	// Init root node
	nodes[0].width = static_cast<short>(w);
	this->nnodes++;
}

fons::Atlas::~Atlas()
{
	
}

int fons::Atlas::InsertNode(int idx, int x, int y, int w)
{
	// Insert node
	if (nnodes + 1 > cnodes) 
	{
		cnodes = cnodes == 0 ? 8 : cnodes * 2;
		nodes = (Node*)realloc(nodes, sizeof(Node) * cnodes);
		if (!nodes)
			return 0;
	}

	for (int i = nnodes; i > idx; i--)
		nodes[i] = nodes[i - 1];
	nodes[idx].x = (short)x;
	nodes[idx].y = (short)y;
	nodes[idx].width = (short)w;
	nnodes++;

	return 1;
}

void fons::Atlas::RemoveNode(int idx)
{
	if (nnodes == 0) return;
	for (int i = idx; i < nnodes - 1; i++)
		nodes[i] = nodes[i + 1];
	nnodes--;
}

void fons::Atlas::Expand(int w, int h)
{
	// Insert node for empty space
	if (w > width)
		InsertNode(nnodes, width, 0, w - width);
	width = w;
	height = h;
}

void fons::Atlas::Reset(int w, int h)
{
	width = w;
	height = h;
	nnodes = 0;

	// Init root node.
	nodes[0].x = 0;
	nodes[0].y = 0;
	nodes[0].width = (short)w;
	nnodes++;
}

int fons::Atlas::AddSkylineLevel(int idx, int x, int y, int w, int h)
{
	// Insert new node
	if (InsertNode(idx, x, y + h, w) == 0)
		return 0;

	// Delete skyline segments that fall under the shadow of the new segment.
	for (int i = idx + 1; i < nnodes; i++) 
	{
		if (nodes[i].x < nodes[i - 1].x + nodes[i - 1].width) 
		{
			int shrink = nodes[i - 1].x + nodes[i - 1].width - nodes[i].x;
			nodes[i].x += (short)shrink;
			nodes[i].width -= (short)shrink;
			if (nodes[i].width <= 0) 
			{
				RemoveNode(i);
				i--;
			}
			else 
			{
				break;
			}
		}
		else 
		{
			break;
		}
	}

	// Merge same height skyline segments that are next to each other.
	for (int i = 0; i < nnodes - 1; i++) 
	{
		if (nodes[i].y == nodes[i + 1].y) 
		{
			nodes[i].width += nodes[i + 1].width;
			RemoveNode(i + 1);
			i--;
		}
	}

	return 1;
}

int fons::Atlas::RectFits(int i, int w, int h)
{
	// Checks if there is enough space at the location of skyline span 'i',
	// and return the max height of all skyline spans under that at that location,
	// (think tetris block being dropped at that position). Or -1 if no space found.
	int x = nodes[i].x;
	int y = nodes[i].y;
	int spaceLeft;
	if (x + w > width)
		return -1;

	spaceLeft = w;
	while (spaceLeft > 0)
	{
		if (i == nnodes) return -1;
		y = fons::maxi(y, nodes[i].y);
		if (y + h > height) return -1;
		spaceLeft -= nodes[i].width;
		++i;
	}
	return y;
}

int fons::Atlas::AddRect(int rw, int rh, int* rx, int* ry)
{
	int besth = height, bestw = width, besti = -1;
	int bestx = -1, besty = -1, i;

	// Bottom left fit heuristic.
	for (i = 0; i < nnodes; i++) 
	{
		int y = RectFits(i, rw, rh);
		if (y != -1) {
			if (y + rh < besth || (y + rh == besth && nodes[i].width < bestw)) 
			{
				besti = i;
				bestw = nodes[i].width;
				besth = y + rh;
				bestx = nodes[i].x;
				besty = y;
			}
		}
	}

	if (besti == -1)
		return 0;

	// Perform the actual packing.
	if (AddSkylineLevel(besti, bestx, besty, rw, rh) == 0)
		return 0;

	*rx = bestx;
	*ry = besty;

	return 1;
}

void fons::Context::AddWhiteRect(int w, int h)
{
	int gx, gy;
	if (atlas->AddRect(w, h, &gx, &gy) == 0)
		return;

	// Rasterize
	u8* dst = &texData[gx + gy * params.width];
	for (int y = 0; y < h; y++) 
	{
		for (int x = 0; x < w; x++)
			dst[x] = 0xff;
		dst += params.width;
	}

	dirtyRect[0] = fons::mini(dirtyRect[0], gx);
	dirtyRect[1] = fons::mini(dirtyRect[1], gy);
	dirtyRect[2] = fons::maxi(dirtyRect[2], gx + w);
	dirtyRect[3] = fons::maxi(dirtyRect[3], gy + h);
}

fons::Context::Context(const Params params)
{
	memset(this, 0, sizeof(*this));

	this->params = params;

	// Allocate scratch buffer.
	scratch = new u8[SCRATCH_BUF_SIZE];

	// Initialize implementation library
	if (!fons__tt_init(this)) throw "fons::Context::Context: fons__tt_init() failed";

	if (params.renderCreate)
		if (params.renderCreate(params.userPtr, params.width, params.height) == 0)
			throw "fons::Context::Context: params.renderCreate() failed";

	atlas = new Atlas(params.width, params.height, INIT_ATLAS_NODES);
	
	// Allocate space for fonts.
	fonts.reserve(INIT_FONTS);

	// Create texture for the cache.
	itw = 1.0f / params.width;
	ith = 1.0f / params.height;
	texData = new u8[params.width * params.height];
	memset(texData, 0, params.width * params.height);

	dirtyRect[0] = params.width;
	dirtyRect[1] = params.height;
	dirtyRect[2] = 0;
	dirtyRect[3] = 0;

	// Add white rect at 0,0 for debug drawing.
	AddWhiteRect(2, 2);

	PushState();
	ClearState();
}

fons::Context::~Context()
{
	if (params.renderDelete)
		params.renderDelete(params.userPtr);

	fonts.clear();
		
	if (atlas) fons__deleteAtlas(atlas);
	if (texData) delete texData;
	if (scratch) delete scratch;
}

fons::State* fons::Context::GetState()
{
	return &states[nstates - 1];
}

void fons::Context::SetSize(float size)
{
	GetState()->size = size;
}

void fons::Context::SetColor(unsigned int color)
{
	GetState()->color = color;
}

void fons::Context::SetSpacing(float spacing)
{
	GetState()->spacing = spacing;
}

void fons::Context::SetBlur(float blur)
{
	GetState()->blur = blur;
}

void fons::Context::SetAlign(int align)
{
	GetState()->align = align;
}

void fons::Context::SetFont(int font)
{
	GetState()->font = font;
}

void fons::Context::PushState()
{
	if (nstates >= MAX_STATES) 
	{
		if (handleError)
			handleError(errorUptr, STATES_OVERFLOW, 0);
		return;
	}
	
	if (nstates > 0)
		memcpy(&states[nstates], &states[nstates - 1], sizeof(State));
	
	nstates++;
}

void fons::Context::PopState()
{
	if (nstates <= 1) 
	{
		if (handleError)
			handleError(errorUptr, STATES_UNDERFLOW, 0);
		return;
	}

	nstates--;
}

void fons::Context::ClearState()
{
	State* state = GetState();
	state->size = 12.0f;
	state->color = 0xffffffff;
	state->font = 0;
	state->blur = 0;
	state->spacing = 0;
	state->align = LEFT | BASELINE;
}

fons::Font::Font()
	: font()
	, data(nullptr)
	, dataSize(0)
	, freeData(0)
	, ascender(0.f)
	, descender(0.f)
	, lineh(0.f)
	, glyphs(nullptr), cglyphs(INIT_GLYPHS), nglyphs(0)
	, lut()
	, fallbacks()
	, nfallbacks(0)
{
	glyphs = new Glyph[INIT_GLYPHS];

	// Init hash lookup.
	memset(&lut, -1, sizeof(int) * HASH_LUT_SIZE);
	memset(&fallbacks, 0, sizeof(int) * MAX_FALLBACKS);
}

fons::Font::~Font()
{
	delete glyphs;
	if (freeData && data) delete data;
}

fons::Glyph* fons::AllocGlyph(Font* font)
{
	if (font->nglyphs + 1 > font->cglyphs) 
	{
		font->cglyphs = font->cglyphs == 0 ? 8 : font->cglyphs * 2;
		font->glyphs = (Glyph*)realloc(font->glyphs, sizeof(Glyph) * font->cglyphs);
		if (!font->glyphs) return nullptr;
	}
	
	font->nglyphs++;
	return &font->glyphs[font->nglyphs - 1];
}


// Based on Exponential blur, Jani Huhtanen, 2006
const int APREC = 16;
const int ZPREC = 7;

void BlurCols(u8* dst, int w, int h, int dstStride, int alpha)
{
	for (int y = 0; y < h; ++y) 
	{
		int z = 0; // force zero border
		for (int x = 1; x < w; ++x) 
		{
			z += (alpha * (((int)(dst[x]) << ZPREC) - z)) >> APREC;
			dst[x] = (u8)(z >> ZPREC);
		}

		dst[w - 1] = 0; // force zero border
		z = 0;

		for (int x = w - 2; x >= 0; --x) 
		{
			z += (alpha * (((int)(dst[x]) << ZPREC) - z)) >> APREC;
			dst[x] = (u8)(z >> ZPREC);
		}
		
		dst[0] = 0; // force zero border
		dst += dstStride;
	}
}

void BlurRows(u8* dst, int w, int h, int dstStride, int alpha)
{
	for (int x = 0; x < w; ++x) 
	{
		int z = 0; // force zero border
		for (int y = dstStride; y < h*dstStride; y += dstStride) 
		{
			z += (alpha * (((int)(dst[y]) << ZPREC) - z)) >> APREC;
			dst[y] = (u8)(z >> ZPREC);
		}
		
		dst[(h - 1)*dstStride] = 0; // force zero border
		z = 0;
		
		for (int y = (h - 2)*dstStride; y >= 0; y -= dstStride) 
		{
			z += (alpha * (((int)(dst[y]) << ZPREC) - z)) >> APREC;
			dst[y] = (u8)(z >> ZPREC);
		}
		
		dst[0] = 0; // force zero border
		dst++;
	}
}


void Blur(u8* dst, int w, int h, int dstStride, int blur)
{
	if (blur < 1) return;
	
	// Calculate the alpha such that 90% of the kernel is within the radius. (Kernel extends to infinity)
	float sigma = (float)blur * 0.57735f; // 1 / sqrt(3)
	int alpha = (int)((1 << APREC) * (1.0f - expf(-2.3f / (sigma + 1.0f))));
	BlurRows(dst, w, h, dstStride, alpha);
	BlurCols(dst, w, h, dstStride, alpha);
	BlurRows(dst, w, h, dstStride, alpha);
	BlurCols(dst, w, h, dstStride, alpha);
}

fons::Glyph* fons::Context::GetGlyph(fons::Font* font, unsigned int codepoint, short isize, short iblur)
{
	if (isize < 2) return NULL;
	if (iblur > 20) iblur = 20;
	int pad = iblur + 2;

	// Reset allocator.
	nscratch = 0;

	// Find code point and size.
	unsigned int h = fons::hashint(codepoint) & (HASH_LUT_SIZE - 1);
	int i = font->lut[h];
	while (i != -1) 
	{
		if (font->glyphs[i].codepoint == codepoint && font->glyphs[i].size == isize && font->glyphs[i].blur == iblur)
			return &font->glyphs[i];
		i = font->glyphs[i].next;
	}

	// Could not find glyph, create it.
	int g = fons__tt_getGlyphIndex(&font->font, codepoint);
	// Try to find the glyph in fallback fonts.
	fons::Font* renderFont = font;
	if (g == 0) 
	{
		for (i = 0; i < font->nfallbacks; ++i) 
		{
			Font* fallbackFont = fonts[font->fallbacks[i]];
			int fallbackIndex = fons__tt_getGlyphIndex(&fallbackFont->font, codepoint);
			if (fallbackIndex != 0) 
			{
				g = fallbackIndex;
				renderFont = fallbackFont;
				break;
			}
		}
		// It is possible that we did not find a fallback glyph.
		// In that case the glyph index 'g' is 0, and we'll proceed below and cache empty glyph.
	}

	float size = isize / 10.0f;
	float scale = fons__tt_getPixelHeightScale(&renderFont->font, size); 
	int advance, lsb, x0, y0, x1, y1;
	fons__tt_buildGlyphBitmap(&renderFont->font, g, size, scale, &advance, &lsb, &x0, &y0, &x1, &y1);
	int gw, gh, gx, gy;
	gw = x1 - x0 + pad * 2;
	gh = y1 - y0 + pad * 2;

	// Find free spot for the rect in the atlas
	int added = atlas->AddRect(gw, gh, &gx, &gy);
	if (added == 0 && handleError) 
	{
		// Atlas is full, let the user to resize the atlas (or not), and try again.
		handleError(errorUptr, fons::ATLAS_FULL, 0);
		added = atlas->AddRect(gw, gh, &gx, &gy);
	}
	if (added == 0) return nullptr;

	// Init glyph.
	Glyph* glyph = AllocGlyph(font);
	glyph->codepoint = codepoint;
	glyph->size = isize;
	glyph->blur = iblur;
	glyph->index = g;
	glyph->x0 = (short)gx;
	glyph->y0 = (short)gy;
	glyph->x1 = (short)(glyph->x0 + gw);
	glyph->y1 = (short)(glyph->y0 + gh);
	glyph->xadv = (short)(scale * advance * 10.0f);
	glyph->xoff = (short)(x0 - pad);
	glyph->yoff = (short)(y0 - pad);
	glyph->next = 0;

	// Insert char to hash lookup.
	glyph->next = font->lut[h];
	font->lut[h] = font->nglyphs - 1;

	// Rasterize
	u8* dst = &texData[(glyph->x0 + pad) + (glyph->y0 + pad) * params.width];
	fons__tt_renderGlyphBitmap(&renderFont->font, dst, gw - pad * 2, gh - pad * 2, params.width, scale, scale, g);

	// Make sure there is one pixel empty border.
	dst = &texData[glyph->x0 + glyph->y0 * params.width];
	for (int y = 0; y < gh; y++) 
	{
		dst[y*params.width] = 0;
		dst[gw - 1 + y*params.width] = 0;
	}
	for (int x = 0; x < gw; x++) 
	{
		dst[x] = 0;
		dst[x + (gh - 1)*params.width] = 0;
	}

	// Blur
	if (iblur > 0) 
	{
		nscratch = 0;
		u8* bdst = &texData[glyph->x0 + glyph->y0 * params.width];
		Blur(bdst, gw, gh, params.width, iblur);
	}

	dirtyRect[0] = fons::mini(dirtyRect[0], glyph->x0);
	dirtyRect[1] = fons::mini(dirtyRect[1], glyph->y0);
	dirtyRect[2] = fons::maxi(dirtyRect[2], glyph->x1);
	dirtyRect[3] = fons::maxi(dirtyRect[3], glyph->y1);

	return glyph;
}

void fons::Context::GetQuad(fons::Font* font, int prevGlyphIndex, fons::Glyph* glyph, float scale, float spacing, float* x, float* y, fons::Quad* q)
{
	if (prevGlyphIndex != -1) 
	{
		float adv = fons__tt_getGlyphKernAdvance(&font->font, prevGlyphIndex, glyph->index) * scale;
		*x += (int)(adv + spacing + 0.5f);
	}

	// Each glyph has 2px border to allow good interpolation,
	// one pixel to prevent leaking, and one to allow good interpolation for rendering.
	// Inset the texture region by one pixel for correct interpolation.
	float xoff = (short)(glyph->xoff + 1);
	float yoff = (short)(glyph->yoff + 1);
	float x0 = (float)(glyph->x0 + 1);
	float y0 = (float)(glyph->y0 + 1);
	float x1 = (float)(glyph->x1 - 1);
	float y1 = (float)(glyph->y1 - 1);

	if (params.flags & ZERO_TOPLEFT) 
	{
		float rx = (float)(int)(*x + xoff);
		float ry = (float)(int)(*y + yoff);

		q->x0 = rx;
		q->y0 = ry;
		q->x1 = rx + x1 - x0;
		q->y1 = ry + y1 - y0;

		q->s0 = x0 * itw;
		q->t0 = y0 * ith;
		q->s1 = x1 * itw;
		q->t1 = y1 * ith;
	}
	else 
	{
		float rx = (float)(int)(*x + xoff);
		float ry = (float)(int)(*y - yoff);

		q->x0 = rx;
		q->y0 = ry;
		q->x1 = rx + x1 - x0;
		q->y1 = ry - y1 + y0;

		q->s0 = x0 * itw;
		q->t0 = y0 * ith;
		q->s1 = x1 * itw;
		q->t1 = y1 * ith;
	}

	*x += (int)(glyph->xadv / 10.0f + 0.5f);
}

void fons::Context::Flush()
{
	// Flush texture
	if (dirtyRect[0] < dirtyRect[2] && dirtyRect[1] < dirtyRect[3]) 
	{
		if (params.renderUpdate)
			params.renderUpdate(params.userPtr, dirtyRect, texData);
		
		// Reset dirty rect
		dirtyRect[0] = params.width;
		dirtyRect[1] = params.height;
		dirtyRect[2] = 0;
		dirtyRect[3] = 0;
	}

	// Flush triangles
	if (nverts > 0) 
	{
		if (params.renderDraw)
			params.renderDraw(params.userPtr, verts, tcoords, colors, nverts);
		nverts = 0;
	}
}

void fons::Context::Vertex(float x, float y, float s, float t, unsigned int c)
{
	verts[nverts * 2 + 0] = x;
	verts[nverts * 2 + 1] = y;
	tcoords[nverts * 2 + 0] = s;
	tcoords[nverts * 2 + 1] = t;
	colors[nverts] = c;
	nverts++;
}

float fons::Context::GetVertAlign(fons::Font* font, int align, short isize)
{
	if (params.flags & ZERO_TOPLEFT) 
	{
		if (align & TOP)
			return font->ascender * (float)isize / 10.0f;
		else if (align & MIDDLE) 
			return (font->ascender + font->descender) / 2.0f * (float)isize / 10.0f;
		else if (align & BASELINE) 
			return 0.0f;
		else if (align & BOTTOM) 
			return font->descender * (float)isize / 10.0f;
	}
	else 
	{
		if (align & TOP) 
			return -font->ascender * (float)isize / 10.0f;
		else if (align & MIDDLE) 
			return -(font->ascender + font->descender) / 2.0f * (float)isize / 10.0f;
		else if (align & BASELINE)
			return 0.0f;
		else if (align & BOTTOM) 
			return -font->descender * (float)isize / 10.0f;
	}

	return 0.f;
}

float fons::Context::DrawText(float x, float y, const char* str, const char* end)
{
	State* state = GetState();

	if (!this) return x;
	if (state->font < 0 || state->font >= fonts.size()) return x;
	Font* font = fonts[state->font];
	if (!font->data) return x;

	short isize = (short)(state->size*10.0f);
	float scale = fons__tt_getPixelHeightScale(&font->font, (float)isize / 10.0f);

	if (!end)
		end = str + strlen(str);

	// Align horizontally
	float width;
	if (state->align & LEFT) 
	{
		// empty
	}
	else if (state->align & RIGHT) 
	{
		width = TextBounds(x, y, str, end, nullptr);
		x -= width;
	}
	else if (state->align & CENTER) 
	{
		width = TextBounds(x, y, str, end, nullptr);
		x -= width * 0.5f;
	}
	// Align vertically.
	y += GetVertAlign(font, state->align, isize);

	int prevGlyphIndex = -1;

	short iblur = (short)state->blur;
	for (; str != end; ++str) 
	{
		unsigned int codepoint;
		unsigned int utf8state = 0;
		if (fons__decutf8(&utf8state, &codepoint, *(const u8*)str))
			continue;
		const auto glyph = GetGlyph(font, codepoint, isize, iblur);
		if (glyph) 
		{
			Quad q;
			GetQuad(font, prevGlyphIndex, glyph, scale, state->spacing, &x, &y, &q);

			if (nverts + 6 > VERTEX_COUNT)
				Flush();
			
			Vertex(q.x0, q.y0, q.s0, q.t0, state->color);
			Vertex(q.x1, q.y1, q.s1, q.t1, state->color);
			Vertex(q.x1, q.y0, q.s1, q.t0, state->color);

			Vertex(q.x0, q.y0, q.s0, q.t0, state->color);
			Vertex(q.x0, q.y1, q.s0, q.t1, state->color);
			Vertex(q.x1, q.y1, q.s1, q.t1, state->color);
		}
		prevGlyphIndex = glyph ? glyph->index : -1;
	}
	
	Flush();

	return x;
}

std::optional<fons::TextIter> fons::Context::TextIterInit(float x, float y, const char* str, const char* end)
{
	State* state = GetState();
	if (state->font < 0 || state->font >= fonts.size()) return {};

	TextIter iter;
	memset(&iter, 0, sizeof(iter));
	iter.font = fonts[state->font];
	if (!iter.font->data) return {};

	iter.isize = (short)(state->size*10.0f);
	iter.iblur = (short)state->blur;
	iter.scale = fons__tt_getPixelHeightScale(&iter.font->font, (float)iter.isize / 10.0f);

	// Align horizontally
	float width;
	if (state->align & LEFT) 
	{
		// empty
	}
	else if (state->align & RIGHT) 
	{
		width = TextBounds(x, y, str, end, nullptr);
		x -= width;
	}
	else if (state->align & CENTER) 
	{
		width = TextBounds(x, y, str, end, nullptr);
		x -= width * 0.5f;
	}
	// Align vertically.
	y += GetVertAlign(iter.font, state->align, iter.isize);

	if (!end)
		end = str + strlen(str);

	iter.x = iter.nextx = x;
	iter.y = iter.nexty = y;
	iter.spacing = state->spacing;
	iter.str = str;
	iter.next = str;
	iter.end = end;
	iter.codepoint = 0;
	iter.prevGlyphIndex = -1;

	return iter;
}

bool fons::Context::TextIterNext(TextIter* iter, Quad* quad)
{
	iter->str = iter->next;

	const char* str = iter->next;
	if (str == iter->end)
		return false;

	for (; str != iter->end; str++)
	{
		if (fons__decutf8(&iter->utf8state, &iter->codepoint, *(const u8*)str))
			continue;
		str++;

		// Get glyph and quad
		iter->x = iter->nextx;
		iter->y = iter->nexty;
		auto glyph = GetGlyph(iter->font, iter->codepoint, iter->isize, iter->iblur);
		if (glyph)
		{
			GetQuad(iter->font, iter->prevGlyphIndex, glyph, iter->scale, iter->spacing, &iter->nextx, &iter->nexty, quad);
			iter->prevGlyphIndex = glyph->index;
		}
		else
		{
			iter->prevGlyphIndex = -1;
		}
		break;
	}
	iter->next = str;

	return true;
}

void fons::Context::DrawDebug(float x, float y)
{
	int w = params.width;
	int h = params.height;
	float u = w == 0 ? 0 : (1.0f / w);
	float v = h == 0 ? 0 : (1.0f / h);

	if (nverts + 6 + 6 > VERTEX_COUNT)
		Flush();

	// Draw background
	Vertex(x + 0, y + 0, u, v, 0x0fffffff);
	Vertex(x + w, y + h, u, v, 0x0fffffff);
	Vertex(x + w, y + 0, u, v, 0x0fffffff);

	Vertex(x + 0, y + 0, u, v, 0x0fffffff);
	Vertex(x + 0, y + h, u, v, 0x0fffffff);
	Vertex(x + w, y + h, u, v, 0x0fffffff);

	// Draw texture
	Vertex(x + 0, y + 0, 0, 0, 0xffffffff);
	Vertex(x + w, y + h, 1, 1, 0xffffffff);
	Vertex(x + w, y + 0, 1, 0, 0xffffffff);

	Vertex(x + 0, y + 0, 0, 0, 0xffffffff);
	Vertex(x + 0, y + h, 0, 1, 0xffffffff);
	Vertex(x + w, y + h, 1, 1, 0xffffffff);

	// Drawbug draw atlas
	for (int i = 0; i < atlas->nnodes; ++i)
	{
		Atlas::Node* n = &atlas->nodes[i];

		if (nverts + 6 > VERTEX_COUNT)
			Flush();

		Vertex(x + n->x + 0, y + n->y + 0, u, v, 0xc00000ff);
		Vertex(x + n->x + n->width, y + n->y + 1, u, v, 0xc00000ff);
		Vertex(x + n->x + n->width, y + n->y + 0, u, v, 0xc00000ff);

		Vertex(x + n->x + 0, y + n->y + 0, u, v, 0xc00000ff);
		Vertex(x + n->x + 0, y + n->y + 1, u, v, 0xc00000ff);
		Vertex(x + n->x + n->width, y + n->y + 1, u, v, 0xc00000ff);
	}

	Flush();
}

float fons::Context::TextBounds(float x, float y, const char* str, const char* end, float* bounds)
{
	State* state = GetState();
	if (state->font < 0 || state->font >= fonts.size()) return 0;
	
	Font* font = fonts[state->font];
	if (!font->data) return 0;
	
	int prevGlyphIndex = -1;
	short isize = (short)(state->size*10.0f);
	short iblur = (short)state->blur;
	float startx, advance;
	float minx, miny, maxx, maxy;

	float scale = fons__tt_getPixelHeightScale(&font->font, (float)isize / 10.0f);

	// Align vertically.
	y += GetVertAlign(font, state->align, isize);

	minx = maxx = x;
	miny = maxy = y;
	startx = x;

	if (!end)
		end = str + strlen(str);

	for (; str != end; ++str) 
	{
		unsigned int codepoint;
		unsigned int utf8state = 0;
		if (fons__decutf8(&utf8state, &codepoint, *(const u8*)str))
			continue;
		const auto glyph = GetGlyph(font, codepoint, isize, iblur);
		if (glyph) 
		{
			Quad q;
			GetQuad(font, prevGlyphIndex, glyph, scale, state->spacing, &x, &y, &q);
			if (q.x0 < minx) minx = q.x0;
			if (q.x1 > maxx) maxx = q.x1;
			if (params.flags & fons::ZERO_TOPLEFT) {
				if (q.y0 < miny) miny = q.y0;
				if (q.y1 > maxy) maxy = q.y1;
			}
			else {
				if (q.y1 < miny) miny = q.y1;
				if (q.y0 > maxy) maxy = q.y0;
			}
		}
		prevGlyphIndex = glyph ? glyph->index : -1;
	}

	advance = x - startx;

	// Align horizontally
	if (state->align & fons::Align::LEFT) 
	{
		// empty
	}
	else if (state->align & fons::Align::RIGHT) 
	{
		minx -= advance;
		maxx -= advance;
	}
	else if (state->align & fons::Align::CENTER) 
	{
		minx -= advance * 0.5f;
		maxx -= advance * 0.5f;
	}

	if (bounds) 
	{
		bounds[0] = minx;
		bounds[1] = miny;
		bounds[2] = maxx;
		bounds[3] = maxy;
	}

	return advance;
}

fons::TextMetrics fons::Context::VertMetrics()
{
	const auto state = GetState();
	const auto font = fonts[state->font];
	
	TextMetrics metrics = { font->ascender, font->descender, font->lineh };
	const auto isize = static_cast<short>(state->size * 10.f);
	metrics.UniformScale(isize / 10.f);
	return metrics;
}

void fons::TextMetrics::UniformScale(const float scale)
{
	ascender *= scale;
	descender *= scale;
	lineh *= scale;
}

void fons::Context::LineBounds(float y, float* miny, float* maxy)
{
	State* state = GetState();
	
	if (!this) return;
	if (state->font < 0 || state->font >= fonts.size()) return;
	Font* font = fonts[state->font];
	short isize = (short)(state->size*10.0f);
	if (!font->data) return;

	y += GetVertAlign(font, state->align, isize);

	if (params.flags & fons::ZERO_TOPLEFT) 
	{
		*miny = y - font->ascender * (float)isize / 10.0f;
		*maxy = *miny + font->lineh*isize / 10.0f;
	}
	else 
	{
		*maxy = y + font->descender * (float)isize / 10.0f;
		*miny = *maxy - font->lineh*isize / 10.0f;
	}
}

const u8* fons::Context::GetTextureData(int* width, int* height)
{
	if (width)
		*width = params.width;
	if (height)
		*height = params.height;
	return texData;
}

int fons::Context::ValidateTexture(int* dirty)
{
	if (dirtyRect[0] < dirtyRect[2] && dirtyRect[1] < dirtyRect[3]) 
	{
		dirty[0] = dirtyRect[0];
		dirty[1] = dirtyRect[1];
		dirty[2] = dirtyRect[2];
		dirty[3] = dirtyRect[3];
		// Reset dirty rect
		dirtyRect[0] = params.width;
		dirtyRect[1] = params.height;
		dirtyRect[2] = 0;
		dirtyRect[3] = 0;
		return 1;
	}
	return 0;
}

void fons::Context::SetErrorCallback(void(*callback)(void* uptr, int error, int val), void* uptr)
{
	handleError = callback;
	errorUptr = uptr;
}

void fons::Context::GetAtlasSize(int* width, int* height)
{
	*width = params.width;
	*height = params.height;
}

int fons::Context::ExpandAtlas(int width, int height)
{
	width = maxi(width, params.width);
	height = maxi(height, params.height);

	if (width == params.width && height == params.height)
		return 1;

	// Flush pending glyphs.
	Flush();

	// Create new texture
	if (params.renderResize) 
	{
		if (params.renderResize(params.userPtr, width, height) == 0)
			return 0;
	}

	// Copy old texture data over.
	auto data = new u8[width * height];
	
	for (int i = 0; i < params.height; i++) 
	{
		u8* dst = &data[i*width];
		u8* src = &texData[i*params.width];
		memcpy(dst, src, params.width);
		if (width > params.width)
			memset(dst + params.width, 0, width - params.width);
	}
	if (height > params.height)
		memset(&data[params.height * width], 0, (height - params.height) * width);

	delete[] texData;
	texData = data;

	// Increase atlas size
	atlas->Expand(width, height);
	
	// Add existing data as dirty.
	int maxy = 0;
	for (int i = 0; i < atlas->nnodes; i++)
		maxy = fons::maxi(maxy, atlas->nodes[i].y);
	dirtyRect[0] = 0;
	dirtyRect[1] = 0;
	dirtyRect[2] = params.width;
	dirtyRect[3] = maxy;

	params.width = width;
	params.height = height;
	itw = 1.0f / params.width;
	ith = 1.0f / params.height;

	return 1;
}

int fons::Context::ResetAtlas(int width, int height)
{
	if (!this) return 0;

	// Flush pending glyphs.
	Flush();

	// Create new texture
	if (params.renderResize) 
	{
		if (params.renderResize(params.userPtr, width, height) == 0)
			return 0;
	}

	// Reset atlas
	atlas->Reset(width, height);
	
	// Clear texture data.
	texData = (u8*)realloc(texData, width * height);
	if (!texData) return 0;
	memset(texData, 0, width * height);

	// Reset dirty rect
	dirtyRect[0] = width;
	dirtyRect[1] = height;
	dirtyRect[2] = 0;
	dirtyRect[3] = 0;

	// Reset cached glyphs
	for(auto& font : fonts)
	{
		font->nglyphs = 0;
		memset(&font->lut, -1, sizeof(int) * HASH_LUT_SIZE);
	}

	params.width = width;
	params.height = height;
	itw = 1.0f / params.width;
	ith = 1.0f / params.height;

	// Add white rect at 0,0 for debug drawing.
	AddWhiteRect(2, 2);

	return 1;
}
