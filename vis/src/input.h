#pragma once


struct GLFWwindow;

struct InputState
{
	GLFWwindow* window;
	double mouseX, mouseY;

	InputState();
	void Update();
};