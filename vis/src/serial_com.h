#pragma once


struct SerialCom
{
    SerialCom(const int comPortNum, const int baudRate);
    ~SerialCom();

    void Listen();
    std::vector<u8> GetData();
    void Write(const std::string& string);

private:
    HANDLE m_handle;
    const int m_comPortNum;
    const int m_baudRate;

    std::mutex m_listenMtx;
    std::vector<u8> m_bytes;
};