#include "stdafx.h"
#include "perf.h"
#include <cstdio>
#include <gl\gl_core.hpp>
#include <nanovg\nanovg.h>
#define GLFW_INCLUDE_NONE
#include <GLFW\glfw3.h>


const GLenum TIME_ELAPSED = 0x88BF;

GPUTimer::GPUTimer()
{
	memset(this, 0, sizeof(*this));
}

void GPUTimer::Start()
{
	if (!supported)
		return;
	gl::BeginQuery(TIME_ELAPSED, queries[cur % GPU_QUERY_COUNT]);
	cur++;
}

int GPUTimer::Stop(float* times, int maxTimes)
{
	NVG_NOTUSED(times);
	NVG_NOTUSED(maxTimes);

	if (!supported)
		return 0;

	gl::EndQuery(TIME_ELAPSED);
	
	GLint available = 1;
	while (available && ret <= cur)
		gl::GetQueryObjectiv(queries[ret % GPU_QUERY_COUNT], gl::QUERY_RESULT_AVAILABLE, &available); // Check for results if there are any
	
	return 0;
}

PerfGraph::PerfGraph(const RenderStyle style_, const std::string& name_)
{
    memset(values, 0, sizeof(float) * GRAPH_HISTORY_COUNT);
	style = style_;
	name = name_;
    head = 0;
}

void PerfGraph::Update(const float frameTime)
{
	head = (head + 1) % GRAPH_HISTORY_COUNT;
	values[head] = frameTime;
}

float PerfGraph::GetAverage()
{
	float avg = 0;
	for (int i = 0; i < GRAPH_HISTORY_COUNT; ++i) 
		avg += values[i];
	return avg / static_cast<float>(GRAPH_HISTORY_COUNT);
}

void PerfGraph::Render(nvg::Context* vg, const float x, const float y)
{
	const float w = 200;
	const float h = 35;

	vg->BeginPath();
	vg->CreateRect(x, y, w, h);
	vg->FillColor(nvg::RGBA(0, 0, 0, 128));
	vg->Fill();
	
	vg->BeginPath();
	vg->MoveTo(x, y + h);
	
	switch (style)
	{
	case RenderStyle::FPS:
		for (size_t i = 0; i < GRAPH_HISTORY_COUNT; ++i)
		{
			float v = 1.0f / (0.00001f + values[(head + i) % GRAPH_HISTORY_COUNT]);
			if (v > 80.0f) v = 80.0f;
			const float vx = x + (static_cast<float>(i) / (GRAPH_HISTORY_COUNT - 1)) * w;
			const float vy = y + h - ((v / 80.0f) * h);
			vg->LineTo(vx, vy);
		}
		break;
	case RenderStyle::PERCENT:
		for (size_t i = 0; i < GRAPH_HISTORY_COUNT; ++i)
		{
			float v = values[(head + i) % GRAPH_HISTORY_COUNT] * 1.0f;
			if (v > 100.0f) v = 100.0f;
			const float vx = x + (static_cast<float>(i) / (GRAPH_HISTORY_COUNT - 1)) * w;
			const float vy = y + h - ((v / 100.0f) * h);
			vg->LineTo(vx, vy);
		}
		break;
	default:
		for (size_t i = 0; i < GRAPH_HISTORY_COUNT; ++i)
		{
			float v = values[(head + i) % GRAPH_HISTORY_COUNT] * 1000.0f;
			if (v > 20.0f) v = 20.0f;
			const float vx = x + (static_cast<float>(i) / (GRAPH_HISTORY_COUNT - 1)) * w;
			const float vy = y + h - ((v / 20.0f) * h);
			vg->LineTo(vx, vy);
		}
		break;
	}

	vg->LineTo(x + w, y + h);
	vg->FillColor(nvg::RGBA(255, 192, 0, 128));
	vg->Fill();

	vg->SetFontFace("sans");
	
	if (!name.empty()) 
	{
		vg->SetFontSize(14.f);
		vg->TextAlign(nvg::Align::LEFT | nvg::Align::TOP);
		vg->FillColor(nvg::RGBA(240, 240, 240, 192));
		vg->Text(x + 3, y + 1, name.c_str(), nullptr);
	}

	const float avg = GetAverage();
	char str[64];

	switch (style)
	{
	case RenderStyle::FPS:
		vg->SetFontSize(18.f);
		vg->TextAlign(nvg::Align::RIGHT | nvg::Align::TOP);
		vg->FillColor(nvg::RGBA(240, 240, 240, 255));
		sprintf(str, "%.2f FPS", 1.0f / avg);
		vg->Text(x + w - 3, y + 1, str, nullptr);

		vg->SetFontSize(15.0f);
		vg->TextAlign(nvg::Align::RIGHT | nvg::Align::BOTTOM);
		vg->FillColor(nvg::RGBA(240, 240, 240, 160));
		sprintf(str, "%.2f ms", avg * 1000.0f);
		vg->Text(x + w - 3, y + h - 1, str, nullptr);
		break;
	case RenderStyle::PERCENT:
		vg->SetFontSize(18.0f);
		vg->TextAlign(nvg::Align::RIGHT | nvg::Align::TOP);
		vg->FillColor(nvg::RGBA(240, 240, 240, 255));
		sprintf(str, "%.1f %%", avg * 1.0f);
		vg->Text(x + w - 3, y + 1, str, nullptr);
		break;
	default:
		vg->SetFontSize(18.0f);
		vg->TextAlign(nvg::Align::RIGHT | nvg::Align::TOP);
		vg->FillColor(nvg::RGBA(240, 240, 240, 255));
		sprintf(str, "%.2f ms", avg * 1000.0f);
		vg->Text(x + w - 3, y + 1, str, nullptr);
		break;
	}
}

PerfPack::PerfPack()
	: dt(0.0)
	, curTime(0.0)
	, fps(PerfGraph::RenderStyle::FPS, "Frame Time")
	, cpuGraph(PerfGraph::RenderStyle::MS, "CPU Time")
	, gpuGraph(PerfGraph::RenderStyle::MS, "GPU Time")
{
	glfwSetTime(0);
	cpuTime = 0;
	prevTime = glfwGetTime();
}

void PerfPack::Reset()
{
	gpuTimer.Start();
	curTime = glfwGetTime();
	dt = curTime - prevTime;
	prevTime = curTime;
}

void PerfPack::Render(nvg::Context* vg)
{
	fps.Render(vg, 5, 5);
	cpuGraph.Render(vg, 5 + 200 + 5, 5);
	if (gpuTimer.supported)
		gpuGraph.Render(vg, 5 + 200 + 5 + 200 + 5, 5);
}

void PerfPack::Elapsed()
{
	cpuTime = glfwGetTime() - curTime;

	fps.Update(dt);
	cpuGraph.Update(cpuTime);

	// We may get multiple results.
	float gpuTimes[3];
	for (int i = 0, n = gpuTimer.Stop(gpuTimes, 3); i < n; ++i)
		gpuGraph.Update(gpuTimes[i]);
}
