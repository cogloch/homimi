#pragma once
#include "radio.hpp"

class IMU;
class BmpSensor;

class Cansat
{
public:
    Cansat();
    void Tick();

private:
    IMU* imu            = nullptr;
    Radio m_radio;
    BmpSensor* bmp      = nullptr;
};
